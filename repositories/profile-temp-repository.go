package repositories

import (
	"gitlab.com/dadard/echo-slam-api/api"
	"gitlab.com/dadard/echo-slam-api/config"
	"gitlab.com/dadard/echo-slam-api/models"
)

func ProfileTempExists(username string) bool {
	_, err := ProfileTempGet(username)
	return err == nil
}

func ProfileTempGet(username string) (models.ProfileTempEntity, error) {
	var fallback models.ProfileTempEntity

	var p models.ProfileTempEntity
	api.Api.Database.Orm.Where(&models.ProfileTempEntity{
		Username: username,
	}).First(&p)

	if p.Username != username {
		return fallback, config.ErrorEntityNotFound
	}

	return p, nil
}

func ProfileTempCreate(p models.ProfileTempEntity) (models.ProfileTempEntity, error) {
	var fallback models.ProfileTempEntity

	api.Api.Database.Orm.Create(&p)

	if !ProfileTempExists(p.Username) {
		return fallback, config.ErrorUnexpectedDb
	}

	return p, nil
}

func ProfileTempDelete(username string) (models.ProfileTempEntity, error) {
	var fallback models.ProfileTempEntity
	var err error

	var p models.ProfileTempEntity
	if p, err = ProfileTempGet(username); err != nil {
		return fallback, err
	}

	api.Api.Database.Orm.Delete(&p)

	if ProfileTempExists(p.Username) {
		return fallback, config.ErrorUnexpectedDb
	}

	return p, nil
}
