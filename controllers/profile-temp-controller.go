package controllers

import (
	"gitlab.com/dadard/echo-slam-api/api"
	"gitlab.com/dadard/echo-slam-api/config"
	"gitlab.com/dadard/echo-slam-api/managers"
	"net/http"
)

// GET
// Authorization: 	None
// Params: 			recoverByParam, contactParam, avatarParam
// Body: 			None

// call asking to create a profile
func ProfileTempPost(w http.ResponseWriter, r *http.Request) {
	// need to confirm recovery settings first

	username, password, ok := r.BasicAuth()
	if !ok {
		err := config.ErrorWrongAuthFormat.Error()
		logger.Error(err)
		api.Api.BuildErrorResponse(http.StatusInternalServerError, err, w)
		return
	}

	recoverBy := r.URL.Query().Get(recoverByParam)
	contact := r.URL.Query().Get(contactParam)
	avatar := r.URL.Query().Get(avatarParam)

	if recoverBy == "" || contact == "" || avatar == "" {
		api.Api.BuildMissingParameter(w)
		return
	}

	// create temp profile in DB and confirmation code
	msg, err := managers.ProfileTempCreate(username, password, recoverBy, contact, avatar)
	if err != nil {
		logger.Error(err.Error())
		api.Api.BuildErrorResponse(http.StatusInternalServerError, msg, w)
		return
	}

	api.Api.BuildJsonResponse(true, msg, nil, w)
}
