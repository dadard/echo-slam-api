package main

import (
	"fmt"
	"github.com/Dadard29/go-api-utils/API"
	"github.com/Dadard29/go-api-utils/database"
	"github.com/Dadard29/go-api-utils/service"
	"gitlab.com/dadard/echo-slam-api/api"
	"gitlab.com/dadard/echo-slam-api/config"
	"gitlab.com/dadard/echo-slam-api/connectors"
	"gitlab.com/dadard/echo-slam-api/controllers"
	"gitlab.com/dadard/echo-slam-api/models"
	"net/http"
	"os"
)

func getRoutes() service.RouteMapping {
	return service.RouteMapping{
		"/health/apis": service.Route{
			Description: "perform health checks on apis",
			MethodMapping: service.MethodMapping{
				http.MethodGet: controllers.HealthCheckApisGet,
			},
		},

		// features
		"/musics": service.Route{
			Description:   "manage musics",
			MethodMapping: service.MethodMapping{
				http.MethodGet: controllers.MusicGet,
			},
		},
		"/musics/search": service.Route{
			Description: "search for musics",
			MethodMapping: service.MethodMapping{
				http.MethodGet: controllers.SearchMusic,
			},
		},
		"/musics/lastadded": service.Route{
			Description: "manage musics list",
			MethodMapping: service.MethodMapping{
				http.MethodGet: controllers.LastAddedMusic,
			},
		},
		"/musics/album/list": service.Route{
			Description:   "manage album list",
			MethodMapping: service.MethodMapping{
				http.MethodGet: controllers.AlbumList,
			},
		},
		"/musics/artist/list": service.Route{
			Description:   "manage artist list",
			MethodMapping: service.MethodMapping{
				http.MethodGet: controllers.ArtistList,
			},
		},
		"/library": service.Route{
			Description:   "manage library",
			MethodMapping: service.MethodMapping{
				http.MethodPost: controllers.LibraryPost,
				http.MethodDelete: controllers.LibraryDelete,
			},
		},
		"/library/album/list": service.Route{
			Description:   "get albums list",
			MethodMapping: service.MethodMapping{
				http.MethodGet: controllers.LibraryAlbumsGet,
			},
		},
		"/library/artist/list": service.Route{
			Description:   "get artists list",
			MethodMapping: service.MethodMapping{
				http.MethodGet: controllers.LibraryArtistGet,
			},
		},
		"/library/download": service.Route{
			Description:   "download all library",
			MethodMapping: service.MethodMapping{
				http.MethodPost: controllers.LibraryDownloadPost,
				http.MethodGet: controllers.LibraryDownloadFileGet,
			},
		},
		"/waiting-list": service.Route{
			Description: "manage waiting list",
			MethodMapping: service.MethodMapping{
				http.MethodGet:    controllers.WaitingListGet,
				http.MethodPost:   controllers.WaitingListPost,
				http.MethodDelete: controllers.WaitingListDelete,
			},
		},
		"/job": service.Route{
			Description: "manage jobs",
			MethodMapping: service.MethodMapping{
				http.MethodPost:   controllers.JobPost,
				http.MethodGet:    controllers.JobGet,
				http.MethodDelete: controllers.JobDelete,
			},
		},
		"/job/done": service.Route{
			Description: "manages done jobs",
			MethodMapping: service.MethodMapping{
				http.MethodGet: controllers.JobDoneGet,
			},
		},

		// profile stuff
		"/profile": service.Route{
			Description: "manage profile",
			MethodMapping: service.MethodMapping{
				http.MethodPost: controllers.ProfilePost,
				http.MethodGet:  controllers.ProfileGet,
			},
		},
		"/profile/public": service.Route{
			Description: "manage public profiles infos",
			MethodMapping: service.MethodMapping{
				http.MethodGet:  controllers.ProfilePublicGet,
				http.MethodPost: controllers.ProfilePublicPost,
			},
		},
		"/profile/public/last": service.Route{
			Description:   "list the last connected users",
			MethodMapping: service.MethodMapping{
				http.MethodGet: controllers.ProfilePublicList,
			},
		},
		"/profile/player": service.Route{
			Description: "manage profile's player",
			MethodMapping: service.MethodMapping{
				http.MethodPut: controllers.PlayerPut,
			},
		},
		"/avatar": service.Route{
			Description: "manage avatars",
			MethodMapping: service.MethodMapping{
				http.MethodGet: controllers.AvatarGet,
			},
		},
		"/avatar/list": service.Route{
			Description: "manage avatars list",
			MethodMapping: service.MethodMapping{
				http.MethodGet: controllers.AvatarListGet,
			},
		},
		"/signup": service.Route{
			Description: "manage profile's creations",
			MethodMapping: service.MethodMapping{
				http.MethodPost: controllers.ProfileTempPost,
			},
		},
		"/signin": service.Route{
			Description: "manage profile's login (jwt stuff)",
			MethodMapping: service.MethodMapping{
				http.MethodGet:  controllers.ProfileSignInGet,
				http.MethodPost: controllers.ProfileSignInPost,
			},
		},
	}
}

// -- env
// DEBUG
// PROD
// CORS_ORIGIN
// DB_USER, DB_PASSWORD
// VERSION
// JWT_SECRET
// BOT_TOKEN
// SMTP_USER, SMTP_PASSWORD
// WAREHOUSE_TOKEN
func main() {
	var err error

	verbose := false
	if os.Getenv("DEBUG") == "1" {
		verbose = true
	}

	isProd := os.Getenv("PROD") == "1"
	configPath := "config/DEV/config.json"
	if isProd {
		configPath = "config/PRD/config.json"
	}

	api.Api = API.NewAPI("ECHO-SLAM-API", configPath, getRoutes(), verbose)
	dbConfig, err := api.Api.Config.GetSubcategoryFromFile(config.Api, config.ApiDb)
	api.Api.Logger.CheckErr(err)


	api.Api.Logger.Info(fmt.Sprintf("-- PROD: %v", isProd))
	api.Api.Logger.Info(fmt.Sprintf("-- DEBUG: %v", verbose))
	api.Api.Logger.Info("-- loaded config from " + configPath)

	// -- init db
	api.Api.Database = database.NewConnector(dbConfig, verbose, []interface{}{
		models.ProfileEntity{},
		models.ProfileTempEntity{},
		models.Job{},
	})

	// -- init connectors
	err = connectors.SetConnectors()
	api.Api.Logger.CheckErr(err)

	api.Api.Service.Start()
	err = api.Api.Service.Stop()

	api.Api.Logger.CheckErr(err)
}
