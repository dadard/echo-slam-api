package connectors_models

type ProfileResponse struct {
	Message string  `json:"Message"`
	Content Profile `json:"Content"`
}

type Profile struct {
	ProfileKey string `json:"ProfileKey"`
}
