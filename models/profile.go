package models

import (
	"gitlab.com/dadard/echo-slam-api/config"
	"gitlab.com/dadard/echo-slam-api/connectors"
	"golang.org/x/crypto/bcrypt"
	"time"
)

// ------------------- entity
type ProfileEntity struct {
	Username       string    `gorm:"type:varchar(70);index:username;primary_key"`
	Password       string    `gorm:"type:varchar(70);index:password"`
	DateRegistered time.Time `gorm:"type:date;index:date_registered"`
	RecoverBy      string    `gorm:"type:varchar(70);index:recovery_by"`
	Contact        string    `gorm:"type:varchar(70);index:contact"`
	DateConnected  time.Time `gorm:"type:datetime;index:date_connected"`
	AvatarName     string    `gorm:"type:varchar(200);index:avatar_name"`

	YoutubeDownloadToken string `gorm:"type:varchar(70);index:youtube_download_token"`
	MusicResearcherToken string `gorm:"type:varchar(70);index:music_researcher_token"`
	MusicLibraryToken    string `gorm:"type:varchar(70);index:music_library_token"`

	PlayingTitle  string `gorm:"type:varchar(100);index:playing_title"`
	PlayingArtist string `gorm:"type:varchar(100);index:playing_artist"`
	PlayingAlbum  string `gorm:"type:varchar(100);index:playing_album"`
	PlayingUrl    string `gorm:"type:varchar(200);index:playing_url"`
}

func NewProfileEntity(username string, passwordHashed string, recoveryBy string,
	contact string, avatarName string, titlePlaying string, artistPlaying string,
	albumPlaying string, urlPlaying string) (ProfileEntity, error) {

	// assume db has already been checks for duplicates
	var fallback ProfileEntity

	if username == "" || passwordHashed == "" || recoveryBy == "" ||
		contact == "" || avatarName == "" {
		return fallback, config.ErrorEmptyParam
	}

	if !connectors.IsRecoverValid(recoveryBy) {
		return fallback, config.ErrorInvalidRecoverBy
	}

	// assume the password weakness has been checked
	return ProfileEntity{
		Username:       username,
		Password:       passwordHashed,
		DateRegistered: time.Now(),
		RecoverBy:      recoveryBy,
		Contact:        contact,
		DateConnected:  time.Now(),
		AvatarName:     avatarName,

		PlayingTitle:  titlePlaying,
		PlayingArtist: artistPlaying,
		PlayingAlbum:  albumPlaying,
		PlayingUrl:    urlPlaying,
	}, nil

}

func (p ProfileEntity) ToDto() ProfileDto {
	return ProfileDto{
		Username:       p.Username,
		DateRegistered: p.DateRegistered,
		RecoverBy:      p.RecoverBy,
		Contact:        p.Contact,
		DateConnected:  p.DateConnected,
		AvatarUrl:      p.AvatarName,

		PlayingTitle:  p.PlayingTitle,
		PlayingArtist: p.PlayingArtist,
		PlayingAlbum:  p.PlayingAlbum,
		PlayingUrl:    p.PlayingUrl,
	}
}

func (p ProfileEntity) ToPublicDto() ProfilePublicDto {
	return ProfilePublicDto{
		Username:       p.Username,
		DateRegistered: p.DateRegistered,
		DateConnected:  p.DateConnected,
		AvatarUrl:      p.AvatarName,

		PlayingTitle:   p.PlayingTitle,
		PlayingArtist:  p.PlayingArtist,
		PlayingAlbum:   p.PlayingAlbum,
		PlayingUrl:     p.PlayingUrl,
	}
}

func (p ProfileEntity) ComparePassword(passwordClear string) error {
	return bcrypt.CompareHashAndPassword([]byte(p.Password), []byte(passwordClear))
}

func (ProfileEntity) TableName() string {
	return "profile"
}

// ------------------- dto
type ProfileDto struct {
	Username       string    `json:"username"`
	DateRegistered time.Time `json:"date_registered"`
	RecoverBy      string    `json:"recover_by"`
	Contact        string    `json:"contact"`
	DateConnected  time.Time `json:"date_connected"`
	AvatarUrl      string    `json:"avatar_name"`

	PlayingTitle  string `json:"playing_title"`
	PlayingArtist string `json:"playing_artist"`
	PlayingAlbum  string `json:"playing_album"`
	PlayingUrl    string `json:"playing_url"`
}

type ProfilePublicDto struct {
	Username       string    `json:"username"`
	DateRegistered time.Time `json:"date_registered"`
	DateConnected  time.Time `json:"date_connected"`
	AvatarUrl      string    `json:"avatar_name"`

	PlayingTitle  string `json:"playing_title"`
	PlayingArtist string `json:"playing_artist"`
	PlayingAlbum  string `json:"playing_album"`
	PlayingUrl    string `json:"playing_url"`
}
