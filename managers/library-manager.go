package managers

import (
	"fmt"
	"gitlab.com/dadard/echo-slam-api/connectors"
	"gitlab.com/dadard/echo-slam-api/connectors/connectors_models"
	"gitlab.com/dadard/echo-slam-api/models"
	"gitlab.com/dadard/echo-slam-api/repositories"
)

func LibraryAdd(username string, title string, artist string) error {
	p, err := repositories.ProfileGet(username)
	if err != nil {
		return err
	}

	m, err := connectors.WarehouseConnector.GetMusic(getWarehouseToken(), title, artist)
	if err != nil {
		return err
	}

	_, err = connectors.MusicLibraryConnector.AddToLibrary(p.MusicLibraryToken, connectors_models.MusicDto{
		Title:       m.Title,
		Artist:      m.Artist,
		Album:       m.Album,
		PublishedAt: m.PublishedAt,
		Genre:       m.Genre,
		ImageUrl:    m.ImageURL,
	})

	return err
}

func LibraryDelete(username string, title string, artist string) error {
	p, err := repositories.ProfileGet(username)
	if err != nil {
		return err
	}

	_, err = connectors.MusicLibraryConnector.RemoveFromLibrary(p.MusicLibraryToken, title, artist)
	return err
}

func LibraryAlbumsList(username string) ([]models.AlbumDto, error) {
	p, err := repositories.ProfileGet(username)
	if err != nil {
		return nil, err
	}

	l, err := connectors.MusicLibraryConnector.AlbumList(p.MusicLibraryToken)
	if err != nil {
		return nil, err
	}

	var res = make([]models.AlbumDto, 0)
	for _, a := range l {
		var titleList = make([]models.TitleDto, 0)
		for _, t := range a.TitleList {
			titleList = append(titleList, models.TitleDto{
				Name: t,
				Url: connectors.WarehouseConnector.GetFileUrl(t, a.Artist, a.Name),
				InLibrary: true,
			})
		}

		res = append(res, models.AlbumDto{
			Name:      a.Name,
			TitleList: titleList,
			Artist:    a.Artist,
			ImageURL:  a.ImageURL,
		})
	}

	return res, nil
}

func LibraryArtistList(username string) ([]models.ArtistDto, error) {
	p, err := repositories.ProfileGet(username)
	if err != nil {
		return nil, err
	}

	l, err := connectors.MusicLibraryConnector.ArtistList(p.MusicLibraryToken)
	if err != nil {
		return nil, err
	}

	var res = make([]models.ArtistDto, 0)
	for _, a := range l {

		var albumList = make([]models.AlbumDto, 0)
		for _, al := range a.AlbumList {
			var titleList = make([]models.TitleDto, 0)

			for _, t := range al.TitleList {
				titleList = append(titleList, models.TitleDto{
					Name: t,
					Url:  connectors.WarehouseConnector.GetFileUrl(t, a.Name, al.Name),
					InLibrary: true,
				})
			}

			albumList = append(albumList, models.AlbumDto{
				Name:      al.Name,
				TitleList: titleList,
				Artist:    al.Artist,
				ImageURL:  al.ImageURL,
			})

		}

		res = append(res, models.ArtistDto{
			Name:      a.Name,
			AlbumList: albumList,
		})
	}

	return res, nil
}

// synchronous job, because it is supposed to be relatively fast
func LibraryDownload(username string) (string, error) {
	var f string

	p, err := repositories.ProfileGet(username)
	if err != nil {
		return f, err
	}

	jobtype := jobTypeLibraryDownload
	_, err = repositories.JobCreate(username, jobtype)
	if err != nil {
		return f, err
	}

	logger.Debug(fmt.Sprintf("starting job %s for %s", jobtype, username))

	artistList, err := connectors.MusicLibraryConnector.ArtistList(p.MusicLibraryToken)
	if err != nil {
		_, errR := repositories.JobEnd(username)
		logger.CheckErr(errR)

		return f, err
	}

	token := p.MusicLibraryToken

	h, err := repositories.LibraryDownloadInitPlaceholder(token)
	if err != nil {
		_, errR := repositories.JobEnd(username)
		logger.CheckErr(errR)

		return f, err
	}

	for _, artist := range artistList {
		for _, album := range artist.AlbumList {
			for _, title := range album.TitleList {
				body, err := connectors.WarehouseConnector.GetFile(
					title, album.Name, artist.Name)
				err = repositories.LibraryDownloadStoreMp3File(h, token, body,
					title, album.Name, artist.Name)

				if err != nil {
					logger.Warning("failed to store file " + title)
					logger.Warning(err.Error())
					continue
				}
			}
		}
	}

	_, err = repositories.LibraryDownloadZipMp3Files(h, token)
	if err != nil {
		_, errR := repositories.JobEnd(username)
		logger.CheckErr(errR)
		return f, err
	}

	err = repositories.LibraryDownloadCleanMp3Files(h, token)
	if err != nil {
		_, errR := repositories.JobEnd(username)
		logger.CheckErr(errR)
		return f, err
	}

	_, err = repositories.JobEnd(username)
	return h, err
}

func LibraryDownloadFile(username string, token string) (string, error) {
	var f string

	p, err := repositories.ProfileGet(username)
	if err != nil {
		return f, err
	}

	zipPath := repositories.LibraryDownloadGetZipFilePath(token, p.MusicLibraryToken)

	return zipPath, nil
}
