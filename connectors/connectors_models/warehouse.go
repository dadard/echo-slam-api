package connectors_models

import "time"

type WarehouseFileListResponse struct {
	Message string          `json:"Message"`
	Content []WarehouseFile `json:"Content"`
}

type WarehouseFileResponse struct {
	Message string        `json:"Message"`
	Content WarehouseFile `json:"Content"`
}

type WarehouseFile struct {
	Title       string    `json:"title"`
	Artist      string    `json:"artist"`
	Album       string    `json:"album"`
	PublishedAt string    `json:"published_at"`
	Genre       string    `json:"genre"`
	ImageURL    string    `json:"image_url"`
	AddedAt     time.Time `json:"added_at"`
}
