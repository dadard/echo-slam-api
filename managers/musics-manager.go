package managers

import (
	"gitlab.com/dadard/echo-slam-api/connectors"
	"gitlab.com/dadard/echo-slam-api/models"
	"gitlab.com/dadard/echo-slam-api/repositories"
	"time"
)

// use music-researcher
func searchSpotify(token string, q string) ([]models.Music, error) {
	ms, err := connectors.MusicResearcherConnector.SearchSpotify(token, q)
	if err != nil {
		return nil, err
	}

	var res = make([]models.Music, 0)
	for _, m := range ms {

		genre := "unknown"
		if len(m.Genre) > 0 {
			genre = m.Genre[0]
		}

		res = append(res, models.Music{
			Title:       m.Title,
			Album:       m.Album,
			Artist:      m.Artist,
			PublishedAt: m.Date,
			ImageUrl:    m.ImageURL,
			Genres:      genre,
			Available:   false,

			InLibrary: false,
			AddedAt:   time.Time{},
			Url:       m.PreviewURL,
		})
	}

	return res, nil
}

// use warehouse
func searchWarehouse(mlToken string, q string) ([]models.Music, error) {
	ms, err := connectors.WarehouseConnector.SearchFile(getWarehouseToken(), q)
	if err != nil {
		return nil, err
	}

	var res = make([]models.Music, 0)
	for _, m := range ms {
		res = append(res, models.Music{
			Title:       m.Title,
			Album:       m.Album,
			Artist:      m.Artist,
			PublishedAt: m.PublishedAt,
			ImageUrl:    m.ImageURL,
			Genres:      m.Genre,
			Available:   true,

			InLibrary: songInLibrary(mlToken, m.Title, m.Artist),
			AddedAt:   m.AddedAt,
			Url:       connectors.WarehouseConnector.GetFileUrl(m.Title, m.Artist, m.Album),
		})
	}
	return res, nil
}

func SearchMusics(username string, q string) ([]models.Music, string, error) {
	profile, err := repositories.ProfileGet(username)
	if err != nil {
		return nil, "error getting profile", err
	}

	// available
	whMusics, err := searchWarehouse(profile.MusicLibraryToken, q)
	if err != nil {
		return nil, "error getting stored musics results", err
	}

	// in spotify
	spotifyMusics, err := searchSpotify(profile.MusicResearcherToken, q)
	if err != nil {
		return nil, "error getting spotify results", err
	}

	// concatenate all results
	var res = whMusics
	for _, s := range spotifyMusics {
		dup := false
		for _, w := range whMusics {
			if s.Title == w.Title && s.Album == w.Album && s.Artist == w.Artist {
				// if duplicate, skip
				dup = true
				break
			}
		}

		if !dup {
			res = append(res, s)
		}
	}

	// todo
	// check if already in waiting list

	return res, "records retrieved", nil
}

func GetLastAddedMusics() ([]models.Music, string, error) {
	files, err := connectors.WarehouseConnector.GetListLastAdded(getWarehouseToken())
	if err != nil {
		return nil, "error getting list from warehouse", err
	}

	var res = make([]models.Music, 0)
	for _, v := range files {
		res = append(res, models.Music{
			Title:       v.Title,
			Album:       v.Album,
			Artist:      v.Artist,
			PublishedAt: v.PublishedAt,
			ImageUrl:    v.ImageURL,
			Genres:      v.Genre,
			Available:   true,

			InLibrary: false,
			AddedAt:   v.AddedAt,

			Url: connectors.WarehouseConnector.GetFileUrl(v.Title, v.Artist, v.Album),
		})
	}

	return res, "list retrieved", nil
}

func songInLibrary(token string, title string, artist string) bool {
	_, err := connectors.MusicLibraryConnector.GetFromLibrary(token, title, artist)
	return err == nil
}

func GetMusic(username string, title string, artist string) (models.Music, error) {
	var f models.Music

	p, err := repositories.ProfileGet(username)
	if err != nil {
		return f, err
	}

	m, err := connectors.WarehouseConnector.GetMusic(getWarehouseToken(), title, artist)
	if err != nil {
		return f, err
	}

	return models.Music{
		Title:       m.Title,
		Album:       m.Album,
		Artist:      m.Artist,
		PublishedAt: m.PublishedAt,
		ImageUrl:    m.ImageURL,
		Genres:      m.Genre,
		Available:   true,
		InLibrary:   songInLibrary(p.MusicLibraryToken, title, artist),
		AddedAt:     m.AddedAt,
		Url:         connectors.WarehouseConnector.GetFileUrl(m.Title, m.Artist, m.Album),
	}, nil
}

func AlbumList(username string) ([]models.AlbumDto, string, error) {
	p, err := repositories.ProfileGet(username)
	if err != nil {
		return nil, "error getting profile", err
	}

	l, err := connectors.WarehouseConnector.GetListAlbum(getWarehouseToken())
	if err != nil {
		return nil, "failed to get album list from warehouse", err
	}

	var res = make([]models.AlbumDto, 0)
	for _, album := range l {
		var titleList = make([]models.TitleDto, 0)
		for _, t := range album.TitleList {
			titleList = append(titleList, models.TitleDto{
				Name: t,
				Url:  connectors.WarehouseConnector.GetFileUrl(t, album.Artist, album.Name),
				InLibrary: songInLibrary(p.MusicLibraryToken, t, album.Artist),
			})
		}

		res = append(res, models.AlbumDto{
			Name:      album.Name,
			TitleList: titleList,
			Artist:    album.Artist,
			ImageURL:  album.ImageURL,
		})
	}

	return res, "album list retrieved", nil
}

func ArtistList(username string) ([]models.ArtistDto, string, error) {
	p, err := repositories.ProfileGet(username)
	if err != nil {
		return nil, "error getting profile", err
	}

	l, err := connectors.WarehouseConnector.GetListArtist(getWarehouseToken())
	if err != nil {
		return nil, "failed to get artist list from warehouse", err
	}

	var res = make([]models.ArtistDto, 0)
	for _, a := range l {

		var albumList = make([]models.AlbumDto, 0)
		for _, al := range a.AlbumList {
			var titleList = make([]models.TitleDto, 0)

			for _, t := range al.TitleList {
				titleList = append(titleList, models.TitleDto{
					Name:      t,
					Url:       connectors.WarehouseConnector.GetFileUrl(t, a.Name, al.Name),
					InLibrary: songInLibrary(p.MusicLibraryToken, t, a.Name),
				})
			}

			albumList = append(albumList, models.AlbumDto{
				Name:      al.Name,
				TitleList: titleList,
				Artist:    al.Artist,
				ImageURL:  al.ImageURL,
			})

		}

		res = append(res, models.ArtistDto{
			Name:      a.Name,
			AlbumList: albumList,
		})
	}

	return res, "artist list retrieved", nil
}
