package connectors

import "time"

const (
	RecoverByTelegram = "telegram"
	RecoverByEmail    = "email"
)

type RecoverByHandler struct {
	SendCodeForConfirmation func(to string, code string, expirationTime time.Time) error
	SendCodeForRecovery     func(to string, code string, expirationTime time.Time) error
}

var RecoverByMapping = map[string]RecoverByHandler{
	RecoverByTelegram: {
		SendCodeForConfirmation: TelegramSendCodeForConfirmation,
		SendCodeForRecovery:     TelegramSendCodeForRecovery,
	},
	RecoverByEmail: {
		SendCodeForConfirmation: EmailSendCodeForConfirmation,
		SendCodeForRecovery:     EmailSendCodeForRecovery,
	},
}

func IsRecoverValid(recoverBy string) bool {
	_, check := RecoverByMapping[recoverBy]
	return check
}

func getWebsiteLinkMarkdown() string {
	return "[echo-slam.com](https://echo-slam.com)"
}

func getWebsiteLinkHtml() string {
	return "<a href=\"https://echo-slam.com\">echo-slam.com</a>"
}
