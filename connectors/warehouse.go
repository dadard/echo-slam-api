package connectors

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/dadard/echo-slam-api/config"
	"gitlab.com/dadard/echo-slam-api/connectors/connectors_models"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/textproto"
	"net/url"
	"os"
	"path/filepath"
)


type Warehouse interface {
	// connector stuff
	Ping() error
	GetName() string

	SearchFile(token string, q string) ([]connectors_models.WarehouseFile, error)
	GetListLastAdded(token string) ([]connectors_models.WarehouseFile, error)
	GetListAlbum(token string) ([]connectors_models.AlbumDto, error)
	GetListArtist(token string) ([]connectors_models.ArtistDto, error)

	HealthConflicts(token string) error
	GetFileUrl(title string, artist string, album string) string
	AddMusic(token string, filePath string, imageUrl string) (connectors_models.WarehouseFile, error)
	GetMusic(token string, title string, artist string) (connectors_models.WarehouseFile, error)
	GetFile(title string, album string, artist string) (io.ReadCloser, error)
}

func getFileBody(filePath string) (*bytes.Buffer, string, error) {
	var fileParamName = "file"
	var headerF = ""

	// open buffer with file data
	file, err := os.Open(filePath)
	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)
	defer writer.Close()

	// set the part with content-type
	h := make(textproto.MIMEHeader)
	h.Set("Content-Disposition", fmt.Sprintf(`form-data; name="%s"; filename="%s"`, fileParamName, filepath.Base(filePath)))
	h.Set("Content-Type", "audio/mpeg")
	part, err := writer.CreatePart(h)
	if err != nil {
		return nil, headerF, err
	}

	// copy file data into part
	io.Copy(part, file)

	return body, writer.FormDataContentType(), nil
}

func (c *Connector) AddMusic(token string, filePath string, imageUrl string) (connectors_models.WarehouseFile, error) {
	var f connectors_models.WarehouseFile

	body, contentType, err := getFileBody(filePath)
	if err != nil {
		return f, err
	}

	route := "/upload?image_url=" + imageUrl

	r, err := http.NewRequest(http.MethodPost, c.baseUrl+route, body)
	if err != nil {
		return f, err
	}

	r.Header.Set("Content-Type", contentType)
	r.Header.Add(authorizationHeader, token)

	resp, err := c.client.Do(r)
	if err != nil {
		return f, err
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return f, err
	}

	var fileResponse connectors_models.WarehouseFileResponse
	err = json.Unmarshal(data, &fileResponse)
	if err != nil {
		return f, err
	}

	if resp.StatusCode != http.StatusOK {
		return f, errors.New(fmt.Sprintf("invalid status from api: %s, %s", resp.Status, fileResponse.Message))
	}

	return fileResponse.Content, nil
}

func (c *Connector) HealthConflicts(token string) error {
	var h connectors_models.HealthResponse
	err := c.doRequest(http.MethodGet, "/health/conflicts", nil, map[string]string{
		authorizationHeader: token,
	}, nil, &h)

	if err != nil {
		return err
	}

	if !h.Status {
		return config.ErrorPingFailed
	}

	return nil
}

func (c *Connector) SearchFile(token string, q string) ([]connectors_models.WarehouseFile, error) {

	var params = map[string]string{
		"q": q,
	}
	var headers = map[string]string{
		authorizationHeader: token,
	}
	var res connectors_models.WarehouseFileListResponse
	err := c.doRequest(http.MethodGet, "/upload/search", params, headers, nil, &res)
	if err != nil {
		return nil, err
	}

	return res.Content, nil
}

func (c *Connector) GetListLastAdded(token string) ([]connectors_models.WarehouseFile, error) {

	var res connectors_models.WarehouseFileListResponse
	err := c.doRequest(http.MethodGet, "/upload/list/last", nil, map[string]string{
		authorizationHeader: token,
	}, nil, &res)

	if err != nil {
		return nil, err
	}

	return res.Content, nil
}

func (c *Connector) GetFileUrl(title string, artist string, album string) string {
	titleUrl := url.QueryEscape(title)
	artistUrl := url.QueryEscape(artist)
	albumUrl := url.QueryEscape(album)
	return fmt.Sprintf("%s/download?title=%s&artist=%s&album=%s", c.baseUrl, titleUrl, artistUrl, albumUrl)
}

func (c *Connector) GetMusic(token string, title string, artist string) (connectors_models.WarehouseFile, error) {
	var r connectors_models.WarehouseFileResponse
	params := map[string]string{
		"title": title,
		"artist": artist,
	}
	headers := map[string]string{
		authorizationHeader: token,
	}
	err := c.doRequest(http.MethodGet, "/upload", params, headers, nil, &r)
	if err != nil {
		return connectors_models.WarehouseFile{}, err
	}

	return r.Content, nil
}

func (c *Connector) GetListAlbum(token string) ([]connectors_models.AlbumDto, error) {
	headers := map[string]string{
		authorizationHeader: token,
	}

	var res connectors_models.AlbumListResponse
	err := c.doRequest(http.MethodGet, "/upload/list/album", nil, headers, nil, &res)

	if err != nil {
		return nil, err
	}

	return res.Content, nil
}

func (c *Connector) GetListArtist(token string) ([]connectors_models.ArtistDto, error) {
	headers := map[string]string{
		authorizationHeader: token,
	}

	var res connectors_models.ArtistListResponse
	err := c.doRequest(http.MethodGet, "/upload/list/artist", nil, headers, nil, &res)

	if err != nil {
		return nil, err
	}

	return res.Content, nil
}

func (c *Connector) GetFile(title string, album string, artist string) (io.ReadCloser, error) {
	r, err := http.NewRequest(http.MethodGet, c.baseUrl+"/download", nil)
	if err != nil {
		return nil, err
	}

	q, err := url.ParseQuery(r.URL.RawQuery)
	if err != nil {
		return nil, err
	}
	q.Add("title", title)
	q.Add("artist", artist)
	q.Add("album", album)

	r.URL.RawQuery = q.Encode()

	resp, err := c.client.Do(r)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, errors.New("invalid status from api " + resp.Status)
	}

	return resp.Body, nil
}
