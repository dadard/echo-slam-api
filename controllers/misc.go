package controllers

import (
	"github.com/Dadard29/go-api-utils/log"
	"github.com/Dadard29/go-api-utils/log/logLevel"
	"gitlab.com/dadard/echo-slam-api/api"
	"gitlab.com/dadard/echo-slam-api/config"
	"gitlab.com/dadard/echo-slam-api/managers"
	"net/http"
)

var logger = log.NewLogger("CONTROLLER", logLevel.DEBUG)

const (
	avatarNameParam = "name"

	usernameParam       = "username"
	codeParam           = "code"
	authorizationHeader = "Authorization"
	recoverByParam      = "recover_by"
	contactParam        = "contact"
	avatarParam         = "avatar"

	queryParam = "q"

	videoUrlParam = "videoUrl"

	titleParam  = "title"
	artistParam = "artist"
	albumParam  = "album"
	urlParam    = "url"

	fileTokenParam = "token"
)

func checkJwt(w http.ResponseWriter, r *http.Request) bool {
	if _, err := getUsernameFromJwt(w, r); err != nil {
		return false
	}

	return true
}

func getUsernameFromJwt(w http.ResponseWriter, r *http.Request) (string, error) {
	var fallback = ""

	jwt := r.Header.Get(authorizationHeader)
	if jwt == "" {
		api.Api.BuildErrorResponse(http.StatusUnauthorized, "Unauthorized", w)
		logger.Error(config.ErrorEmptyParam.Error())
		return fallback, config.ErrorEmptyParam
	}

	username, msg, err := managers.ProfileManagerSignInJwtCheck(jwt)
	if err != nil {
		logger.Error(err.Error())
		api.Api.BuildErrorResponse(http.StatusUnauthorized, msg, w)
		return fallback, err
	}

	return username, nil
}
