package config

const (
	Api   = "api"
	ApiDb = "db"
)

const (
	Connectors                = "connectors"
	ConnectorsEmail           = "email"
	ConnectorsTelegram        = "telegram"
	ConnectorsYoutubeDownload = "youtube-download"
	ConnectorsMusicResearcher = "music-researcher"
	ConnectorsWarehouse       = "warehouse"
	ConnectorsCore            = "core"
	ConnectorsMusicLibrary    = "music-library"
)

const (
	Profile         = "profile"
	ProfileCreation = "creation"
	ProfileJwt      = "jwt"
	ProfileAvatar   = "avatar"

	ProfilePlayerDefault = "playerDefault"
)
