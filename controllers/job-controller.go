package controllers

import (
	"gitlab.com/dadard/echo-slam-api/api"
	"gitlab.com/dadard/echo-slam-api/managers"
	"net/http"
)

// POST
// Authorization: 	JWT
// Params: 			None
// Body: 			None
func JobPost(w http.ResponseWriter, r *http.Request) {
	username, err := getUsernameFromJwt(w, r)
	if err != nil {
		return
	}

	j, msg, err := managers.JobManagerProcessMusic(username)
	if err != nil {
		logger.Error(err.Error())
		api.Api.BuildErrorResponse(http.StatusInternalServerError, msg, w)
		return
	}

	api.Api.BuildJsonResponse(true, msg, j, w)
}

// GET
// Authorization: 	JWT
// Params: 			None
// Body: 			None
func JobGet(w http.ResponseWriter, r *http.Request) {
	username, err := getUsernameFromJwt(w, r)
	if err != nil {
		return
	}

	j, err := managers.JobManagerProcessMusicStatus(username)
	if err != nil {
		logger.Error(err.Error())
		api.Api.BuildErrorResponse(http.StatusNotFound, "job not found", w)
		return
	}

	api.Api.BuildJsonResponse(true, "status retrieved", j, w)
}

// DELETE
// Authorization: 	JWT
// Params: 			None
// Body: 			None
func JobDelete(w http.ResponseWriter, r *http.Request) {
	username, err := getUsernameFromJwt(w, r)
	if err != nil {
		return
	}

	j, err := managers.JobManagerCancel(username)
	if err != nil {
		logger.Error(err.Error())
		api.Api.BuildErrorResponse(http.StatusInternalServerError, "failed to stop the job", w)
		return
	}
	api.Api.BuildJsonResponse(true, "job cancelled", j, w)
}

// GET
// Authorization: 	Jwt
// Params: 			None
// Body: 			None
func JobDoneGet(w http.ResponseWriter, r *http.Request) {
	username, err := getUsernameFromJwt(w, r)
	if err != nil {
		return
	}

	j, err := managers.JobManagerDoneGet(username)
	if err != nil {
		logger.Error(err.Error())
		api.Api.BuildErrorResponse(http.StatusNotFound, "not done job yet", w)
		return
	}

	api.Api.BuildJsonResponse(true, "last done job retrieved", j, w)
}
