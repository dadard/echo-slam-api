package models

import "time"

type Job struct {
	Id        string    `gorm:"type:int;index:id;primary_key;auto_increment"`
	Username  string    `gorm:"type:varchar(70);index:username"`
	Type      string    `gorm:"type:varchar(40);index:type"`
	Progress  int       `gorm:"type:int;index:progress"`
	Done      bool      `gorm:"type:bool;index:done"`
	Status    string    `gorm:"type:varchar(100);index:status"`
	StartedAt time.Time `gorm:"type:datetime;index:started_at"`
	EndedAt   time.Time `gorm:"type:datetime;index:ended_at"`
}

func (Job) TableName() string {
	return "job"
}

func (j Job) ToDto() JobDto {
	return JobDto{
		Type:      j.Type,
		Progress:  j.Progress,
		Done:      j.Done,
		Status:    j.Status,
		StartedAt: j.StartedAt,
		EndedAt:   j.EndedAt,
	}
}

type JobDto struct {
	Type      string    `json:"type"`
	Progress  int       `json:"progress"`
	Done      bool      `json:"done"`
	Status    string    `json:"status"`
	StartedAt time.Time `json:"started_at"`
	EndedAt   time.Time `json:"ended_at"`
}
