package managers

import (
	"errors"
	"fmt"
	"gitlab.com/dadard/echo-slam-api/config"
	"gitlab.com/dadard/echo-slam-api/connectors"
	"gitlab.com/dadard/echo-slam-api/connectors/connectors_models"
	"gitlab.com/dadard/echo-slam-api/models"
	"gitlab.com/dadard/echo-slam-api/repositories"
	"time"
)

func compareVideoToMusic(v connectors_models.Video, m models.Music) bool {
	vDate, _ := v.GetDate()
	mDate, _ := m.GetPublishedTime()

	return v.Title == m.Title && v.Artist == m.Artist && v.Album == m.Album &&
		vDate == mDate && v.Genre == m.Genres
}

// get current waiting list
func WaitingListManagerGetList(username string) ([]models.Music, error) {
	p, err := repositories.ProfileGet(username)
	if err != nil {
		return nil, err
	}

	ytToken := p.YoutubeDownloadToken

	l, err := connectors.YoutubeDownloadConnector.ListVideos(ytToken)
	if err != nil {
		return nil, err
	}

	var res = make([]models.Music, 0)
	for _, v := range l {
		res = append(res, models.Music{
			Title:       v.Title,
			Album:       v.Album,
			Artist:      v.Artist,
			PublishedAt: v.Date,
			ImageUrl:    v.ImageUrl,
			Genres:      v.Genre,
			Available:   false,
			InLibrary:   false,
			AddedAt:     time.Time{},
			Url:         "https://youtu.be/" + v.VideoID,
		})
	}

	return res, nil
}

// remove video from waiting list
func WaitingListManagerDeleteMusic(username string, title string, artist string, album string) error {
	p, err := repositories.ProfileGet(username)
	if err != nil {
		return err
	}

	token := p.YoutubeDownloadToken

	l, err := connectors.YoutubeDownloadConnector.ListVideos(token)
	if err != nil {
		return err
	}

	for _, v := range l {
		if v.Title == title && v.Artist == artist && v.Album == album {
			_, err = connectors.YoutubeDownloadConnector.DeleteVideo(token, v.VideoID)
			if err != nil {
				return err
			}
			return nil
		}
	}

	return errors.New("music not found in waiting list")
}

// add input music in waiting list
func WaitingListManagerAddMusic(username string, music models.Music) error {
	// check if not in warehouse yet
	_, err := connectors.WarehouseConnector.GetMusic(getWarehouseToken(), music.Title, music.Artist)
	if err == nil {
		// already in warehouse
		return config.ErrorMusicAlreadyExists
	}

	p, err := repositories.ProfileGet(username)
	if err != nil {
		return err
	}

	ytToken := p.YoutubeDownloadToken

	// format publishedAt
	publishedAtTime, err := music.GetPublishedTime()
	if err != nil {
		return err
	}
	publishedAtStr := fmt.Sprintf("%02d-%02d-%02d", publishedAtTime.Year(), publishedAtTime.Month(), publishedAtTime.Day())

	// add to ytdl list
	v, err := connectors.YoutubeDownloadConnector.
		AddVideoFromSearch(ytToken, music.Title, music.Artist,music.Album, music.Genres, publishedAtStr)
	if err != nil {
		return err
	}

	// update entry with music metadatas
	t, err := music.GetPublishedTime()
	if err != nil {
		return err
	}

	vUpdated, err := connectors.YoutubeDownloadConnector.UpdateVideo(ytToken, v.VideoID, music.Title,
		music.Artist, music.Album, t, music.Genres, music.ImageUrl)
	if err != nil {
		_, errd := connectors.YoutubeDownloadConnector.DeleteVideo(ytToken, v.VideoID)
		logger.CheckErr(errd)
		return err
	}

	if !compareVideoToMusic(vUpdated, music) {
		_, errd := connectors.YoutubeDownloadConnector.DeleteVideo(ytToken, v.VideoID)
		logger.CheckErr(errd)
		return errors.New("failed to set the music metadata")
	}

	return nil
}
