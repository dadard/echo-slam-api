package managers

import (
	"github.com/Dadard29/go-api-utils/auth"
	"gitlab.com/dadard/echo-slam-api/api"
	"gitlab.com/dadard/echo-slam-api/config"
	"gitlab.com/dadard/echo-slam-api/connectors"
	"gitlab.com/dadard/echo-slam-api/models"
	"gitlab.com/dadard/echo-slam-api/repositories"
	"strconv"
	"time"
)

const (
	listProfileLimit = 5
)

func getCipheredJwt(username string) (string, error) {
	var fallback = ""
	jwtConfig, err := api.Api.Config.GetSubcategoryFromFile(
		config.Profile,
		config.ProfileJwt)
	if err != nil {
		return fallback, err
	}

	// set up params
	jwtSecretKey := jwtConfig["secretKey"]
	jwtSecret := api.Api.Config.GetEnv(jwtSecretKey)

	jwtDurationStr := jwtConfig["duration"]
	jwtDurationInt, err := strconv.Atoi(jwtDurationStr)
	if err != nil {
		return fallback, err
	}

	issuer := jwtConfig["issuer"]
	subject := jwtConfig["subject"]
	audience := []string{jwtConfig["audience"]}

	// config is given with day unit
	jwtDuration := time.Duration(jwtDurationInt) * time.Hour * 24

	payload := models.JwtPayload{
		Username: username,
	}

	// create JWT
	jwt, err := auth.NewJwtHS256(jwtSecret, issuer, subject, audience, jwtDuration, payload)
	if err != nil {
		return fallback, err

	}

	// cipher JWT
	keyFile := jwtConfig["keyFile"]
	cipheredJwt, err := auth.CipherJwtWithJwe(keyFile, jwt)
	if err != nil {
		return fallback, err
	}

	return string(cipheredJwt), nil
}

func getDefaultPlayerSettings() (map[string]string, error) {
	return api.Api.Config.GetSubcategoryFromFile(config.Profile, config.ProfilePlayerDefault)
}

// create profile in core
func ProfileManagerCreateAndSubscribe(username string, password string,
	recoverBy string, contact string) (map[string]string, error) {

	p, err := connectors.CoreConnector.CreateProfile(username, password, recoverBy, contact)
	if err != nil {
		return nil, err
	}

	logger.Info("profile created in core")

	pk := p.ProfileKey
	subList, err := connectors.CoreConnector.Subscribe(pk,
		connectors.CoreConnector.GetApiList())
	if err != nil {
		return nil, err
	}

	logger.Info("profile subscriptions done")

	var resList = map[string]string{}
	for _, v := range subList {
		resList[v.Api.Name] = v.AccessToken
	}

	return resList, nil
}

// set the tokens foreach api for a user
func ProfileUpdateSubsToken(username string, tokens map[string]string) error {
	var ytdlToken, mrToken, mlToken string

	var check bool
	if ytdlToken, check = tokens[config.ConnectorsYoutubeDownload]; !check {
		return config.ErrorEmptyParam
	}

	if mrToken, check = tokens[config.ConnectorsMusicResearcher]; !check {
		return config.ErrorEmptyParam
	}

	if mlToken, check = tokens[config.ConnectorsMusicLibrary]; !check {
		return config.ErrorEmptyParam
	}

	p, err := repositories.ProfileGet(username)
	if err != nil {
		return err
	}

	p.YoutubeDownloadToken = ytdlToken
	p.MusicResearcherToken = mrToken
	p.MusicLibraryToken = mlToken

	repositories.ProfileUpdate(p)

	return nil
}

// confirm recovery and create profile in DB
func ProfileManagerCreate(username string, password string,
	code string) (models.ProfileDto, string, error) {

	var err error
	var fallback models.ProfileDto
	var msg = "error creating profile"

	// check for duplicates
	if repositories.ProfileExists(username) {
		return fallback, "username already used", config.ErrorUsernameAlreadyUsed
	}

	// get the temp profile
	profileTempDb, err := repositories.ProfileTempGet(username)
	if err != nil {
		return fallback, "wrong username/password", err
	}

	// check expiration
	if time.Now().After(profileTempDb.ExpireAt) {
		// code is expired
		// delete the current temp profile
		if _, err := repositories.ProfileTempDelete(profileTempDb.Username); err != nil {
			logger.Warning(err.Error())
		}

		return fallback, "code expired", config.ErrorCodeExpired
	}

	// check passwords
	if err := profileTempDb.ComparePassword(password); err != nil {
		return fallback, "wrong username/password", config.ErrorWrongPassword
	}

	// check code
	if profileTempDb.Code != code {
		return fallback, "wrong code", config.ErrorWrongCode
	}

	// check performed, do the addition
	// instantiate
	m, err := getDefaultPlayerSettings()
	if err != nil {
		return fallback, "error setting player", err
	}
	newProfile, err := models.NewProfileEntity(
		profileTempDb.Username,
		profileTempDb.Password,
		profileTempDb.RecoverBy,
		profileTempDb.Contact,
		profileTempDb.AvatarName,
		m["title"], m["artist"], m["album"], m["url"])
	if err != nil {
		return fallback, msg, err
	}

	// create
	newProfileDb, err := repositories.ProfileCreate(newProfile)
	if err != nil {
		return fallback, msg, err
	}

	// delete the temp profile
	_, err = repositories.ProfileTempDelete(newProfileDb.Username)
	if err != nil {
		return fallback, msg, err
	}

	// ez pz
	return newProfileDb.ToDto(), "profile created", nil

}

// give a JWT
func ProfileManagerSignIn(username string, password string) (string, string, error) {
	var fallback string
	var msg = "wrong username/password"
	var err error

	// check profile exists
	p, err := repositories.ProfileGet(username)
	if err != nil {
		return fallback, msg, err
	}

	// check password
	if err = p.ComparePassword(password); err != nil {
		return fallback, msg, err
	}

	// update date connected
	p.DateConnected = time.Now()
	repositories.ProfileUpdate(p)

	// generate JWT
	jwt, err := getCipheredJwt(username)
	if err != nil {
		return fallback, "error setting JWT", err
	}

	return jwt, "jwt created", nil
}

// check a JWT validity
func ProfileManagerSignInJwtCheck(jwtCiphered string) (string, string, error) {
	var err error
	var fallback = ""

	keyFile, err := api.Api.Config.GetValueFromFile(
		config.Profile,
		config.ProfileJwt,
		"keyFile")

	deciphered, err := auth.DecipherJwtWithJwe(keyFile, []byte(jwtCiphered))
	if err != nil {
		return fallback, "invalid token", err
	}

	jwtSecretKey, err := api.Api.Config.GetValueFromFile(
		config.Profile,
		config.ProfileJwt,
		"secretKey")
	if err != nil {
		return fallback, "error validating token", err
	}
	payload, err := auth.VerifyJwtHS256(deciphered, api.Api.Config.GetEnv(jwtSecretKey))
	if err != nil {
		return fallback, "invalid token", err
	}
	payloadMap := payload.Infos.(map[string]interface{})
	username := payloadMap["Username"].(string)

	return username, "valid token", nil

}

// give private profile DB
func ProfileManagerGet(username string) (models.ProfileDto, string, error) {
	var fallback models.ProfileDto

	profileDb, err := repositories.ProfileGet(username)
	if err != nil {
		return fallback, "error getting profile", err
	}

	return profileDb.ToDto(), "profile retrieved", nil
}

// delete profile from DB
func ProfileManagerDelete(username string) (string, error) {
	err := repositories.ProfileDelete(username)
	if err != nil {
		return "error while deleting profile", err
	}

	return "profile deleted", nil
}

// give public profile DB
func ProfileManagerPublicGet(username string) (models.ProfilePublicDto, string, error) {
	var fallback models.ProfilePublicDto

	profileDb, err := repositories.ProfileGet(username)
	if err != nil {
		return fallback, "error getting profile", err
	}

	return profileDb.ToPublicDto(), "profile retrieved", nil
}

// give list of last connected users
func ProfileManagerPublicList(username string) ([]models.ProfilePublicDto, string, error) {
	l := repositories.ProfileList(listProfileLimit)

	var res = make([]models.ProfilePublicDto, 0)
	for _, p := range l {
		if p.Username == username {
			continue
		}
		res = append(res, p.ToPublicDto())
	}

	return res, "profile list retrieved", nil
}

// check if profile exists in DB
func ProfileManagerExists(username string) bool {
	return repositories.ProfileExists(username)
}
