package connectors

import (
	"fmt"
	"os"
	"testing"
)

func TestWarehouseAddFile(t *testing.T) {
	var c Warehouse = newConnector("warehouse", "http://localhost:8084")

	f, err := c.AddMusic(os.Getenv("WAREHOUSE_TOKEN"), ".trash/lighthouse.mp3", "image_url")
	if err != nil {
		t.Error(err)
	}

	fmt.Println(f.Title)
}
