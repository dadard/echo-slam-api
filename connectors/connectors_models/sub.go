package connectors_models

type SubscriptionResponse struct {
	Message string         `json:"Message"`
	Content []Subscription `json:"Content"`
}

type Subscription struct {
	AccessToken string          `json:"AccessToken"`
	Api         SubscriptionApi `json:"Api"`
}

type SubscriptionApi struct {
	Name string `json:"Name"`
}
