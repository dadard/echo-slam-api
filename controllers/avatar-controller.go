package controllers

import (
	"gitlab.com/dadard/echo-slam-api/api"
	"gitlab.com/dadard/echo-slam-api/managers"
	"net/http"
)

// GET
// Authorization: 	None
// Params: 			None
// Body: 			None

// give the list of avatars URL
func AvatarListGet(w http.ResponseWriter, r *http.Request) {
	l, msg, err := managers.AvatarManagerList()
	if err != nil {
		logger.Error(err.Error())
		api.Api.BuildErrorResponse(http.StatusInternalServerError, msg, w)
		return
	}

	api.Api.BuildJsonResponse(true, msg, l, w)
}

// GET
// Authorization: 	None
// Params: 			avatarParam
// Body: 			None

// serve an avatar file
func AvatarGet(w http.ResponseWriter, r *http.Request) {
	name := r.URL.Query().Get(avatarNameParam)
	if name == "" {
		api.Api.BuildMissingParameter(w)
		return
	}

	p, msg, err := managers.AvatarManagerGet(name)
	if err != nil {
		logger.Error(err.Error())
		api.Api.BuildErrorResponse(http.StatusInternalServerError, msg, w)
		return
	}

	http.ServeFile(w, r, p)

}
