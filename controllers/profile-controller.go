package controllers

import (
	"gitlab.com/dadard/echo-slam-api/api"
	"gitlab.com/dadard/echo-slam-api/config"
	"gitlab.com/dadard/echo-slam-api/managers"
	"net/http"
)

// -- public

// GET
// Authorization: 	JWT
// Params: 			usernameParam
// Body: 			None

// public profile infos, still need to be connected to see profiles
func ProfilePublicGet(w http.ResponseWriter, r *http.Request) {
	if !checkJwt(w, r) {
		return
	}

	username := r.URL.Query().Get(usernameParam)

	if username == "" {
		api.Api.BuildMissingParameter(w)
		return
	}

	p, msg, err := managers.ProfileManagerPublicGet(username)

	if err != nil {
		logger.Error(err.Error())
		api.Api.BuildErrorResponse(http.StatusInternalServerError, msg, w)
		return
	}

	api.Api.BuildJsonResponse(true, msg, p, w)
}

// POST
// Authorization: 	None
// Params: 			usernamePara
// Body: 			None

// check if a username is available
func ProfilePublicPost(w http.ResponseWriter, r *http.Request) {
	username := r.URL.Query().Get(usernameParam)

	if username == "" {
		api.Api.BuildMissingParameter(w)
		return
	}

	if managers.ProfileTempExists(username) || managers.ProfileManagerExists(username) {
		api.Api.BuildJsonResponse(true, "username used", true, w)
	} else {
		api.Api.BuildJsonResponse(true, "username free", false, w)
	}
}

// GET
// Authorization: 	JWT
// Params: 			None
// Body: 			None

// list the last connected users
func ProfilePublicList(w http.ResponseWriter, r *http.Request) {
	username, err := getUsernameFromJwt(w, r)
	if err != nil {
		return
	}

	l, msg, err := managers.ProfileManagerPublicList(username)
	if err != nil {
		logger.Error(err.Error())
		api.Api.BuildErrorResponse(http.StatusInternalServerError, msg, w)
		return
	}

	api.Api.BuildJsonResponse(true, msg, l, w)
}

// -- private

// POST
// Authorization: 	Basic
// Params: 			codeParam
// Body: 			None

// create a profile entity from a temp profile
func ProfilePost(w http.ResponseWriter, r *http.Request) {
	// called after retrieved a confirmation code

	username, password, ok := r.BasicAuth()
	if !ok {
		err := config.ErrorWrongAuthFormat.Error()
		logger.Error(err)
		api.Api.BuildErrorResponse(http.StatusInternalServerError, err, w)
		return
	}

	code := r.URL.Query().Get(codeParam)
	if code == "" {
		api.Api.BuildMissingParameter(w)
		return
	}

	// check the code and create the real profile in DB
	p, msg, err := managers.ProfileManagerCreate(username, password, code)
	if err != nil {
		logger.Error(err.Error())
		api.Api.BuildErrorResponse(http.StatusInternalServerError, msg, w)
		return
	}

	// create the profile in the core and subscribe
	tokensList, err := managers.ProfileManagerCreateAndSubscribe(
		username, password, p.RecoverBy, p.Contact)

	if err != nil {
		logger.Error(err.Error())

		// delete the newly created profile
		if _, err := managers.ProfileManagerDelete(username); err != nil {
			logger.Error(err.Error())
		}
		api.Api.BuildErrorResponse(http.StatusInternalServerError,
			"error creating profile in core", w)
		return
	}

	if err = managers.ProfileUpdateSubsToken(username, tokensList); err != nil {
		logger.Error(err.Error())

		// delete the newly created profile
		if _, err := managers.ProfileManagerDelete(username); err != nil {
			logger.Error(err.Error())
		}
		api.Api.BuildErrorResponse(http.StatusInternalServerError,
			"error setting tokens", w)
		return
	}

	api.Api.BuildJsonResponse(true, msg, p, w)
}

// GET
// Authorization: 	Basic
// Params: 			None
// Body: 			None

// authenticate the user and gives a JWT ciphered
func ProfileSignInGet(w http.ResponseWriter, r *http.Request) {
	username, password, ok := r.BasicAuth()
	if !ok {
		err := config.ErrorWrongAuthFormat.Error()
		logger.Error(err)
		api.Api.BuildErrorResponse(http.StatusInternalServerError, err, w)
		return
	}

	jwt, msg, err := managers.ProfileManagerSignIn(username, password)
	if err != nil {
		logger.Error(err.Error())
		api.Api.BuildErrorResponse(http.StatusInternalServerError, msg, w)
		return
	}

	api.Api.BuildJsonResponse(true, msg, jwt, w)
}

// POST
// Authorization: 	JWT
// Params: 			None
// Body: 			None

// check if JWT is valid
func ProfileSignInPost(w http.ResponseWriter, r *http.Request) {
	username, err := getUsernameFromJwt(w, r)
	if err != nil {
		return
	}

	api.Api.BuildJsonResponse(true, "token valid", username, w)
}

// GET
// Authorization: 	JWT
// Params: 			None
// Body: 			None

// get profile
func ProfileGet(w http.ResponseWriter, r *http.Request) {
	username, err := getUsernameFromJwt(w, r)
	if err != nil {
		return
	}

	p, msg, err := managers.ProfileManagerGet(username)
	if err != nil {
		logger.Error(err.Error())
		api.Api.BuildErrorResponse(http.StatusInternalServerError, msg, w)
		return
	}

	api.Api.BuildJsonResponse(true, msg, p, w)
}

// DELETE
// Authorization: 	JWT
// Params: 			None
// Body: 			None

// delete profile here and in core
func ProfileDelete(w http.ResponseWriter, r *http.Request) {
	// todo
}

// PUT
// Authorization: 	JWT
// Params: 			newPassword
// Body: 			None
func ProfileChangePassword(w http.ResponseWriter, r *http.Request) {
	// todo
}
