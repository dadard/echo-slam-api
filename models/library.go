package models


// alternative representation of models.MusicDto
type AlbumDto struct {
	Name string   `json:"Name"`
	TitleList []TitleDto `json:"TitleList"`
	Artist    string   `json:"Artist"`
	ImageURL  string   `json:"ImageUrl"`
}

type ArtistDto struct {
	Name string `json:"Name"`
	AlbumList  []AlbumDto `json:"AlbumList"`
}

type TitleDto struct {
	Name string
	Url string
	InLibrary bool
}
