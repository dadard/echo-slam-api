package managers

import (
	"github.com/Dadard29/go-api-utils/log"
	"github.com/Dadard29/go-api-utils/log/logLevel"
	"gitlab.com/dadard/echo-slam-api/api"
)

var logger = log.NewLogger("MANAGER", logLevel.DEBUG)

func getWarehouseToken() string {
	return api.Api.Config.GetEnv("WAREHOUSE_TOKEN")
}
