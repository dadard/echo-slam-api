package connectors

import (
	coreConnectors "github.com/Dadard29/go-core/connectors"
	"time"
)

func formatTelegramMessageMarkdown(title string, content string) string {
	return "" +
		"*" + title + "*\n\n" +
		"Hello my dude, Earthshaker here,\n\n" +
		content + "\n\n" +
		"Have a pleasant day, and shake it,\n\n" +
		"_The echo-slam.com support_"

}

func TelegramSendCodeForConfirmation(to string, code string, expirationTime time.Time) error {

	formattedExpirationTime := expirationTime.Format("15:04:05")

	content := "" +
		"It seems you have requested an account creation on " +
		getWebsiteLinkMarkdown() + "\n\n" +
		"To proceed, please use this code to confirm your identity\n\n" +
		"*" + code + "*\n\n" +
		"Acknowledge that this code will expire at " + formattedExpirationTime + "\n"

	msg := formatTelegramMessageMarkdown("Account creation", content)

	return Telegram.SendMessage(msg, to, coreConnectors.ParseModeMarkdown)
}

func TelegramSendCodeForRecovery(to string, code string, expirationTime time.Time) error {
	formattedExpirationTime := expirationTime.Format("15:04:05")

	content := "" +
		"It seems you have requested the recovery of your account on " +
		getWebsiteLinkMarkdown() + "\n\n" +
		"To proceed, please use this code to confirm your identity\n\n" +
		"*" + code + "*\n\n" +
		"Acknowledge that this code will expire at " + formattedExpirationTime + "\n"

	msg := formatTelegramMessageMarkdown("Account recovery", content)

	return Telegram.SendMessage(msg, to, coreConnectors.ParseModeMarkdown)
}
