package controllers

import (
	"gitlab.com/dadard/echo-slam-api/api"
	"gitlab.com/dadard/echo-slam-api/managers"
	"net/http"
)

// GET
// Authorization: 	jwt
// Params: 			queryParam
// Body: 			None
func SearchMusic(w http.ResponseWriter, r *http.Request) {
	var username string
	var err error

	if username, err = getUsernameFromJwt(w, r); err != nil {
		return
	}

	query := r.URL.Query().Get(queryParam)
	if query == "" {
		api.Api.BuildMissingParameter(w)
		return
	}

	musics, msg, err := managers.SearchMusics(username, query)
	if err != nil {
		logger.Error(err.Error())
		api.Api.BuildErrorResponse(http.StatusInternalServerError, msg, w)
		return
	}

	api.Api.BuildJsonResponse(true, msg, musics, w)
}

// GET
// Authorization: 	jwt
// Params: 			None
// Body: 			None
func LastAddedMusic(w http.ResponseWriter, r *http.Request) {
	var err error

	if !checkJwt(w, r) {
		return
	}

	musics, msg, err := managers.GetLastAddedMusics()
	if err != nil {
		logger.Error(err.Error())
		api.Api.BuildErrorResponse(http.StatusInternalServerError, msg, w)
		return
	}

	api.Api.BuildJsonResponse(true, "list retrieved", musics, w)
}

// GET
// Authorization: 	jwt
// Params: 			titleParam, artistParam
// Body: 			None
func MusicGet(w http.ResponseWriter, r *http.Request) {
	username, err := getUsernameFromJwt(w, r)
	if err != nil {
		return
	}

	title := r.URL.Query().Get(titleParam)
	artist := r.URL.Query().Get(artistParam)

	if title == "" || artist == "" {
		api.Api.BuildMissingParameter(w)
		return
	}

	m, err := managers.GetMusic(username, title, artist)
	if err != nil {
		logger.Error(err.Error())
		api.Api.BuildErrorResponse(http.StatusInternalServerError, "failed to get the music", w)
		return
	}

	api.Api.BuildJsonResponse(true, "music retrieved", m, w)

}

// GET
// Authorization: 	jwt
// Params: 			None
// Body: 			None

// get the list of albums available
func AlbumList(w http.ResponseWriter, r *http.Request) {
	var username string
	var err error
	if username, err = getUsernameFromJwt(w, r); err != nil {
		return
	}

	l, msg, err := managers.AlbumList(username)
	if err != nil {
		logger.Error(err.Error())
		api.Api.BuildErrorResponse(http.StatusInternalServerError, msg, w)
		return
	}
	api.Api.BuildJsonResponse(true, msg, l, w)
}

// GET
// Authorization: 	jwt
// Params: 			None
// Body: 			None

// get the list of artist available
func ArtistList(w http.ResponseWriter, r *http.Request) {
	var username string
	var err error
	if username, err = getUsernameFromJwt(w, r); err != nil {
		return
	}

	l, msg, err := managers.ArtistList(username)
	if err != nil {
		logger.Error(err.Error())
		api.Api.BuildErrorResponse(http.StatusInternalServerError, msg, w)
		return
	}

	api.Api.BuildJsonResponse(true, msg, l, w)
}
