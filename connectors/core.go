package connectors

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/dadard/echo-slam-api/config"
	"gitlab.com/dadard/echo-slam-api/connectors/connectors_models"
	"net/http"
	"os"
)

const (
	echoSlamTokenKey = "ECHO_SLAM_TOKEN"
	echoSlamHeader   = "X-Echo-Slam"
)

type Core interface {
	// connector stuff
	Ping() error
	GetName() string

	getEchoSlamToken() string

	CreateProfile(username string, password string,
		recoverBy string, contact string) (*connectors_models.Profile, error)
	Subscribe(profileKey string, apiList []string) ([]connectors_models.Subscription, error)
	GetApiList() []string
}

func (c *Connector) getEchoSlamToken() string {
	return os.Getenv(echoSlamTokenKey)
}

func (c *Connector) CreateProfile(username string, password string,
	recoverBy string, contact string) (*connectors_models.Profile, error) {
	route := fmt.Sprintf("/profile/echo-slam?recover_by=%s&contact=%s", recoverBy, contact)

	req, err := http.NewRequest(http.MethodPost, c.baseUrl+route, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add(echoSlamHeader, c.getEchoSlamToken())
	req.SetBasicAuth(username, password)

	resp, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, errors.New(fmt.Sprintf("invalid status code from api: %s", resp.Status))
	}

	var pRes connectors_models.ProfileResponse
	if err := deserialize(resp, &pRes); err != nil {
		return nil, err
	}

	p := pRes.Content

	return &p, nil
}

func (c *Connector) Subscribe(profileKey string, apiList []string) ([]connectors_models.Subscription, error) {
	route := "/subs/echo-slam?profileKey=" + profileKey

	bodyData, err := json.Marshal(apiList)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(http.MethodPost, c.baseUrl+route, bytes.NewBuffer(bodyData))
	if err != nil {
		return nil, err
	}

	req.Header.Add(echoSlamHeader, c.getEchoSlamToken())

	resp, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, errors.New(fmt.Sprintf("invalid status code from api: %s", resp.Status))
	}

	var sRes connectors_models.SubscriptionResponse
	if err := deserialize(resp, &sRes); err != nil {
		return nil, err
	}

	subList := sRes.Content

	return subList, nil
}

func (c *Connector) GetApiList() []string {
	return []string{
		config.ConnectorsYoutubeDownload,
		config.ConnectorsMusicResearcher,
		config.ConnectorsMusicLibrary,
	}
}
