package models

import (
	"gitlab.com/dadard/echo-slam-api/api"
	"gitlab.com/dadard/echo-slam-api/config"
	"gitlab.com/dadard/echo-slam-api/connectors"
	"golang.org/x/crypto/bcrypt"
	"math/rand"
	"strconv"
	"time"
)

// entity
type ProfileTempEntity struct {
	Username   string    `gorm:"type:varchar(70);index:username;primary_key"`
	Password   string    `gorm:"type:varchar(70);index:password"`
	RecoverBy  string    `gorm:"type:varchar(70);index:recovery_by"`
	Contact    string    `gorm:"type:varchar(70);index:contact"`
	AvatarName string    `gorm:"type:varchar(200);index:avatar_name"`
	ExpireAt   time.Time `gorm:"type:datetime;index:expire_at"`
	Code       string    `gorm:"type:varchar(10);index:code"`
}

func (ProfileTempEntity) TableName() string {
	return "profile_temp"
}

func generateCode() string {
	const charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

	seed := time.Now().UnixNano()
	rand.Seed(seed)
	code := ""
	for i := 0; i < 5; {
		randInt := rand.Intn(len(charset))
		c := string(charset[randInt])
		code += c
		i += 1
	}

	return code
}

func NewProfileTempEntity(username string, passwordClear string,
	recoveryBy string, contact string, avatar string) (ProfileTempEntity, string, error) {

	var fallback ProfileTempEntity
	var msg = "error creating temp profile"

	// empty check
	if username == "" || passwordClear == "" || recoveryBy == "" || contact == "" || avatar == "" {
		return fallback, "empty entry", config.ErrorEmptyParam
	}

	// recovery option check
	if !connectors.IsRecoverValid(recoveryBy) {
		return fallback, "error creating profile", config.ErrorInvalidRecoverBy
	}

	// weakness check (basic)
	// todo
	if len([]byte(passwordClear)) < 8 {
		return fallback, "password is too weak", config.ErrorPasswordWeak
	}

	// hashing
	bcryptPassword, err := bcrypt.GenerateFromPassword([]byte(passwordClear), 14)
	if err != nil {
		return fallback, msg, err
	}

	// computing expiration date
	expirationDurationStr, err := api.Api.Config.GetValueFromFile(
		config.Profile,
		config.ProfileCreation,
		"expiration")
	if err != nil {
		return fallback, msg, err
	}

	expirationDuration, err := strconv.Atoi(expirationDurationStr)
	if err != nil {
		return fallback, msg, err
	}

	expirationTime := time.Now().Add(time.Second * time.Duration(expirationDuration))

	// creating obj
	return ProfileTempEntity{
		Username:   username,
		Password:   string(bcryptPassword),
		RecoverBy:  recoveryBy,
		Contact:    contact,
		AvatarName: avatar,
		ExpireAt:   expirationTime,
		Code:       generateCode(),
	}, "profile creation ongoing", nil

}

func (p ProfileTempEntity) ComparePassword(passwordClear string) error {
	return bcrypt.CompareHashAndPassword([]byte(p.Password), []byte(passwordClear))
}

func (p ProfileTempEntity) IsExpired() bool {
	return time.Now().After(p.ExpireAt)
}
