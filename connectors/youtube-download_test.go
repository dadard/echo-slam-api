package connectors

import (
	"fmt"
	"os"
	"testing"
	"time"
)

func newYtdl() *Connector {
	return newConnector("youtube-download", "http://localhost:8082")
}

func getYtdlToken() string {
	return os.Getenv("YTDL_TOKEN")
}

var videoId = "BbFnBlRErLg"
var playlistId = "PLijJv7pr2YCtcAzBl7qdlBcXAQouHCyjF"
var videoIdList = []string{"WCM_ZD5oXIY", "2uOwR7n8YwA", "k7Z7USWo2Lk", "ITfNdrP0Vn0",
	"F2V9xKySdi0", "5rOq5ilGfUo", "pDSoWXyrnLM", "w2Ov5jzm3j8"}

func TestConnector_ListVideos(t *testing.T) {
	var c YoutubeDownload = newYtdl()

	l, err := c.ListVideos(getYtdlToken())
	if err != nil {
		t.Error(err)
	}

	fmt.Println(l)
}

func TestConnector_AddVideo(t *testing.T) {
	var c YoutubeDownload = newYtdl()
	v, err := c.AddVideo(getYtdlToken(), videoId)
	if err != nil {
		t.Error(err)
	}

	fmt.Println(v)
}

func TestConnector_UpdateVideo(t *testing.T) {
	var c YoutubeDownload = newYtdl()
	v, err := c.UpdateVideo(getYtdlToken(), videoId, "newTitle", "newArtist",
		"newAlbum", time.Now(), "newGenre", "newImageUrl")
	if err != nil {
		t.Error(err)
	}

	fmt.Println(v)
}

func TestConnector_DeleteVideo(t *testing.T) {
	var c YoutubeDownload = newYtdl()
	v, err := c.DeleteVideo(getYtdlToken(), videoId)
	if err != nil {
		t.Error(err)
	}

	fmt.Println(v)
}

func TestConnector_AddPlaylist(t *testing.T) {
	c := newYtdl()
	l, err := c.AddPlaylist(getYtdlToken(), playlistId)
	if err != nil {
		t.Error(err)
	}

	fmt.Println(l)
}

func TestConnector_UpdateList(t *testing.T) {
	c := newYtdl()
	l, err := c.UpdateList(getYtdlToken(), videoIdList, "newTitle",
		"newArtist", "newAlbum", time.Now(), "newGenre")
	if err != nil {
		t.Error(err)
	}

	fmt.Println(l)
}

func TestConnector_DeleteList(t *testing.T) {
	c := newYtdl()
	l, err := c.DeleteList(getYtdlToken(), videoIdList)
	if err != nil {
		t.Error(err)
	}

	fmt.Println(l)
}

func TestConnector_GetDownloadStatus(t *testing.T) {
	c := newYtdl()
	l, err := c.GetDownloadStatus(getYtdlToken())
	if err != nil {
		t.Error(err)
	}

	fmt.Println(l)
}
