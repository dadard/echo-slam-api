package controllers

import (
	"encoding/json"
	"gitlab.com/dadard/echo-slam-api/api"
	"gitlab.com/dadard/echo-slam-api/managers"
	"gitlab.com/dadard/echo-slam-api/models"
	"io/ioutil"
	"net/http"
)

// GET
// Authorization: 	JWT
// Params: 			None
// Body: 			None

// get the waiting list
func WaitingListGet(w http.ResponseWriter, r *http.Request) {
	username, err := getUsernameFromJwt(w, r)
	if err != nil {
		return
	}

	l, err := managers.WaitingListManagerGetList(username)
	if err != nil {
		logger.Error(err.Error())
		api.Api.BuildErrorResponse(http.StatusInternalServerError, "error getting list", w)
		return
	}

	api.Api.BuildJsonResponse(true, "list retrieved", l, w)
}

// POST
// Authorization: 	JWT
// Params: 			none
// Body: 			models.Music

// add a video to the waiting list
func WaitingListPost(w http.ResponseWriter, r *http.Request) {
	username, err := getUsernameFromJwt(w, r)
	if err != nil {
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		logger.Error(err.Error())
		api.Api.BuildErrorResponse(http.StatusBadRequest, "invalid body", w)
		return
	}

	var m models.Music
	err = json.Unmarshal(body, &m)
	if err != nil {
		logger.Error(err.Error())
		api.Api.BuildErrorResponse(http.StatusBadRequest, "invalid json body", w)
		return
	}

	if !m.CheckSanity() {
		api.Api.BuildMissingParameter(w)
		return
	}

	err = managers.WaitingListManagerAddMusic(username, m)
	if err != nil {
		logger.Error(err.Error())
		api.Api.BuildErrorResponse(http.StatusInternalServerError, "error adding video", w)
		return
	}

	api.Api.BuildJsonResponse(true,
		"video added to waiting list, checkout your dashboard", nil, w)
}

// DELETE
// Authorization: 	JWT
// Params: 			titleParam, artistParam, albumParam
// Body: 			None

// remove a music from waiting list
func WaitingListDelete(w http.ResponseWriter, r *http.Request) {
	username, err := getUsernameFromJwt(w, r)
	if err != nil {
		return
	}

	title := r.URL.Query().Get(titleParam)
	artist := r.URL.Query().Get(artistParam)
	album := r.URL.Query().Get(albumParam)

	if title == "" || artist == "" || album == "" {
		api.Api.BuildMissingParameter(w)
		return
	}

	err = managers.WaitingListManagerDeleteMusic(username, title, artist, album)
	if err != nil {
		logger.Error(err.Error())
		api.Api.BuildErrorResponse(http.StatusInternalServerError, "error removing music", w)
		return
	}
	api.Api.BuildJsonResponse(true, "music removed from waiting list", nil, w)
}
