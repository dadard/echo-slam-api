package connectors_models

import "time"

type JobResponse struct {
	Status  bool   `json:"Status"`
	Message string `json:"Message"`
	Content Job    `json:"Content"`
}

type Job struct {
	DateStarted  time.Time `json:"DateStarted"`
	DateFinished time.Time `json:"DateFinished"`
	Progress     int       `json:"Progress"`
	Done         bool      `json:"Done"`
	Message      string    `json:"Message"`
}
