package managers

import (
	"errors"
	"fmt"
	"gitlab.com/dadard/echo-slam-api/connectors"
	"gitlab.com/dadard/echo-slam-api/connectors/connectors_models"
	"gitlab.com/dadard/echo-slam-api/models"
	"gitlab.com/dadard/echo-slam-api/repositories"
	"time"
)

const (
	jobStatusDownloadMusic           = "downloading_all"
	jobStatusTransferringToWarehouse = "storing_all"
	jobStatusAddingToLibrary         = "adding_to_library"
	jobStatusCleaningWaitingList = "cleaning_waiting_list"
)

const (
	jobTypeWaitingListProcess = "waiting_list_process"
	jobTypeLibraryDownload = "library_download"

	jobSleepDuration          = 2 * time.Second

	jobDownloadAllWeight = 50
	jobUploadAllWeight   = 25
	jobAddAllWeight      = 15
	jobCleanListWeight = 10
)

func logJobStatus(j models.Job) {
	logger.Debug(fmt.Sprintf("status %s\t; progression %d\t;", j.Status, j.Progress))
}

func downloadAll(ytToken string, username string) error {
	s, err := connectors.YoutubeDownloadConnector.DownloadAll(ytToken)
	if err != nil {
		return err
	}

	if !s.Status {
		return errors.New("failed to start downloading all")
	}

	// -- 0%
	logger.Debug("starting downloading all")
	j, err := repositories.JobUpdateProgress(username, 0, jobStatusDownloadMusic)
	logger.CheckErr(err)
	logJobStatus(j)

	// check status and update it in DB
	jobStatus, err := connectors.YoutubeDownloadConnector.GetDownloadStatus(ytToken)
	logger.CheckErr(err)

	for !jobStatus.Done {
		// sleeping
		time.Sleep(jobSleepDuration)

		// check job still exists
		if !repositories.JobExists(username) {
			return errors.New("job has been cancelled")
		}

		// update progression
		jobStatus, err = connectors.YoutubeDownloadConnector.GetDownloadStatus(ytToken)
		logger.CheckErr(err)

		newProgress := jobStatus.Progress * jobDownloadAllWeight / 100
		j, err := repositories.JobUpdateProgress(username, newProgress, jobStatusDownloadMusic)
		logger.CheckErr(err)
		logJobStatus(j)
	}

	// -- 50%

	return nil
}

func uploadAll(ytToken string, whToken string, username string) ([]models.Music, error) {

	logger.Debug("downloading zip file")
	j, err := repositories.JobUpdateProgress(username, jobDownloadAllWeight, jobStatusTransferringToWarehouse)
	logger.CheckErr(err)
	logJobStatus(j)

	body, err := connectors.YoutubeDownloadConnector.GetZipFile(ytToken)
	if err != nil {
		return nil, err
	}

	// storing body into zip file
	h, err := repositories.StoreZipFile(body)
	if err != nil {
		err := repositories.ClearPlaceholder(h)
		logger.CheckErr(err)
		return nil, err
	}

	// extract zip content
	err = repositories.ExtractZipFile(h)
	if err != nil {
		err := repositories.ClearPlaceholder(h)
		logger.CheckErr(err)
		return nil, err
	}

	// upload each files
	waitingList, err := WaitingListManagerGetList(username)
	if err != nil {
		err := repositories.ClearPlaceholder(h)
		logger.CheckErr(err)
		return nil, err
	}

	count := 0
	whList := make([]models.Music, 0)
	for _, m := range waitingList {
		// get the corresponding file
		f, err := repositories.GetMp3File(h, m)
		if err != nil {
			logger.Warning("failed to get file for title " + m.Title)
			logger.Warning(err.Error())
			continue
		}

		whFile, err := connectors.WarehouseConnector.AddMusic(whToken, f, m.ImageUrl)
		if err != nil {
			logger.Warning("failed to upload file " + f)
			logger.Warning(err.Error())
			continue
		}

		whList = append(whList, models.Music{
			Title:       whFile.Title,
			Album:       whFile.Album,
			Artist:      whFile.Artist,
			PublishedAt: whFile.PublishedAt,
			ImageUrl:    whFile.ImageURL,
			Genres:      whFile.Genre,
			Available:   true,
			InLibrary:   false,
			AddedAt:     whFile.AddedAt,
			Url: connectors.WarehouseConnector.GetFileUrl(
				whFile.Title, whFile.Artist, whFile.Album),
		})

		// update job progression
		count += 1
		newProgress := jobDownloadAllWeight + (count * jobUploadAllWeight / len(waitingList))
		j, err := repositories.JobUpdateProgress(username, newProgress, jobStatusTransferringToWarehouse)
		logger.CheckErr(err)
		logJobStatus(j)
	}

	// clear placeholder
	err = repositories.ClearPlaceholder(h)
	logger.CheckErr(err)

	// -- 75%
	j, err = repositories.JobUpdateProgress(username, jobDownloadAllWeight+jobUploadAllWeight,
		jobStatusTransferringToWarehouse)
	logger.CheckErr(err)
	logJobStatus(j)

	return whList, nil
}

func addAllToLibrary(mlToken string, musics []models.Music, username string) ([]models.Music, error) {

	j, err := repositories.JobUpdateProgress(username, jobDownloadAllWeight+jobUploadAllWeight,
		jobStatusAddingToLibrary)
	logger.CheckErr(err)
	logJobStatus(j)

	count := 0
	addedMusic := make([]models.Music, 0)
	for _, m := range musics {

		_, err := connectors.MusicLibraryConnector.AddToLibrary(mlToken, connectors_models.MusicDto{
			Title:       m.Title,
			Artist:      m.Artist,
			Album:       m.Album,
			PublishedAt: m.PublishedAt,
			Genre:       m.Genres,
			ImageUrl:    m.ImageUrl,
		})

		if err != nil {
			logger.Warning(fmt.Sprintf("failed to add music with title %s to library", m.Title))
			logger.Warning(err.Error())
			continue
		}

		count += 1
		newProgress := count * jobAddAllWeight / len(musics)
		j, err := repositories.JobUpdateProgress(
			username,
			jobDownloadAllWeight+jobUploadAllWeight+newProgress,
			jobStatusAddingToLibrary)

		logger.CheckErr(err)
		logJobStatus(j)

		addedMusic = append(addedMusic, m)

	}

	j, err = repositories.JobUpdateProgress(
		username,
		jobDownloadAllWeight+jobUploadAllWeight+jobAddAllWeight,
		jobStatusAddingToLibrary)
	logger.CheckErr(err)
	logJobStatus(j)

	return addedMusic, nil
}

func cleanWaitingList(ytToken string, addedMusics []models.Music, username string) error {
	j, err := repositories.JobUpdateProgress(
		username,
		jobDownloadAllWeight+jobUploadAllWeight+jobAddAllWeight,
		jobStatusCleaningWaitingList)
	logger.CheckErr(err)
	logJobStatus(j)

	wl, err := connectors.YoutubeDownloadConnector.ListVideos(ytToken)
	if err != nil {
		return err
	}

	vList :=  make([]string, 0)
	for _, v := range wl {
		for _, m := range addedMusics {
			if m.Title == v.Title && m.Artist == v.Artist && m.Album == v.Album {
				// video found in added musics
				vList = append(vList, v.VideoID)
			}
			// else, dont clean
		}
	}


	_, err = connectors.YoutubeDownloadConnector.DeleteList(ytToken, vList)

	j, err = repositories.JobUpdateProgress(
		username,
		jobDownloadAllWeight+jobUploadAllWeight+jobAddAllWeight+jobCleanListWeight,
		jobStatusCleaningWaitingList)
	logger.CheckErr(err)
	logJobStatus(j)

	return err
}

func mainProcess(p models.ProfileEntity, username string) {
	go func() {
		// download all into zip file
		err := downloadAll(p.YoutubeDownloadToken, username)
		if err != nil {
			logger.Error(err.Error())

			// stop the current running job
			_, err := repositories.JobEnd(username)
			logger.CheckErr(err)
			return
		}

		// get zipfile and unzip, upload all files into warehouse
		musics, err := uploadAll(p.YoutubeDownloadToken, getWarehouseToken(), username)
		if err != nil {
			logger.Error(err.Error())

			// stop the current running job
			_, err := repositories.JobEnd(username)
			logger.CheckErr(err)
			return
		}

		// add all music into library
		addedMusics, err := addAllToLibrary(p.MusicLibraryToken, musics, username)
		if err != nil {
			logger.Error(err.Error())

			// stop the current running job
			_, err := repositories.JobEnd(username)
			logger.CheckErr(err)
			return
		}

		// clean the current waiting list
		err = cleanWaitingList(p.YoutubeDownloadToken, addedMusics, username)
		if err != nil {
			logger.Error(err.Error())

			// stop the current running job
			_, err := repositories.JobEnd(username)
			logger.CheckErr(err)
			return
		}

		// end the job
		j, err := repositories.JobEnd(username)
		if err != nil {
			logger.Error(err.Error())
			return
		}
		logJobStatus(j)
	}()
}

// download waiting-list, upload to warehouse and add to library
func JobManagerProcessMusic(username string) (models.JobDto, string, error) {
	var f models.JobDto
	p, err := repositories.ProfileGet(username)
	if err != nil {
		return f, "error getting profile", err
	}

	j, err := repositories.JobCreate(username, jobTypeWaitingListProcess)
	if err != nil {
		return f, "error starting job", err
	}

	logger.Debug("starting job for " + username)

	// launch process
	mainProcess(p, username)

	return j.ToDto(), "waiting list processing started", nil
}

// get status of running job
func JobManagerProcessMusicStatus(username string) (models.JobDto, error) {
	var f models.JobDto

	// retrieve job status from db
	j, err := repositories.JobGet(username)
	if err != nil {
		return f, err
	}

	return j.ToDto(), nil
}

// cancel current running job
func JobManagerCancel(username string) (models.JobDto, error) {
	var f models.JobDto

	j, err := repositories.JobEnd(username)
	if err != nil {
		return f, err
	}

	return j.ToDto(), nil
}

// get the last done job of user
func JobManagerDoneGet(username string) (models.JobDto, error) {
	var f models.JobDto

	j, err := repositories.JobDoneGet(username)
	if err != nil {
		return f, err
	}

	return j.ToDto(), nil
}
