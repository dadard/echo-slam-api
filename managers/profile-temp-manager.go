package managers

import (
	"gitlab.com/dadard/echo-slam-api/config"
	"gitlab.com/dadard/echo-slam-api/connectors"
	"gitlab.com/dadard/echo-slam-api/models"
	"gitlab.com/dadard/echo-slam-api/repositories"
)

func ProfileTempCreate(username string, password string,
	recoverBy string, contact string, avatar string) (string, error) {

	var err error
	var msg = "error creating temp profile"

	// check for duplicates in existing profiles
	if repositories.ProfileExists(username) {
		return "username already used", config.ErrorUsernameAlreadyUsed
	}

	// check for duplicates in temp profiles
	if repositories.ProfileTempExists(username) {
		// check if the existing temp profile is expired
		tempProfile, _ := repositories.ProfileTempGet(username)
		if tempProfile.IsExpired() {

			// expired, delete the existing and proceed
			logger.Debug("deleting existing temp profile: " + tempProfile.Username)
			repositories.ProfileTempDelete(tempProfile.Username)
		} else {

			// not expired, still waiting
			return "username already used", config.ErrorUsernameAlreadyUsed
		}
	}

	// check avatar validity
	if _, msg, err := AvatarManagerGet(avatar); err != nil {
		return msg, err
	}

	// instantiate
	newProfileTemp, m, err := models.NewProfileTempEntity(username, password, recoverBy, contact, avatar)
	if err != nil {
		return m, err
	}

	// send code
	var handler connectors.RecoverByHandler
	var check bool

	// should be already checks in NewProfileTempEntity, but anyway, we never know what can happen
	if handler, check = connectors.RecoverByMapping[recoverBy]; !check {
		return msg, config.ErrorInvalidRecoverBy
	}

	// validate contact value
	if err = handler.SendCodeForConfirmation(newProfileTemp.Contact,
		newProfileTemp.Code, newProfileTemp.ExpireAt); err != nil {
		return "error sending code", err
	}

	// create in db, waiting for confirmation
	if _, err := repositories.ProfileTempCreate(newProfileTemp); err != nil {
		return msg, err
	}

	return "temp profile created, waiting for confirmation", nil
}

func ProfileTempExists(username string) bool {
	return repositories.ProfileTempExists(username)
}
