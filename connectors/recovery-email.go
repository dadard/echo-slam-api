package connectors

import "time"

func formatEmailMessageHtml(title string, content string) string {
	return "<h3>" + title + "</h1>" +
		"<p>Hello my dude, Eartshaker here,</p>" +
		"<p>" +
		content +
		"</p>" +
		"<p>Have a pleasant day, and shake it</p>" +
		"<i>The echo-slam support</i>"
}

func EmailSendCodeForConfirmation(to string, code string, expirationTime time.Time) error {
	formattedExpirationTime := expirationTime.Format("15:04:05")

	content := "" +
		"It seems you have requested an account creation on " +
		getWebsiteLinkHtml() + ". " +
		"To proceed, please use this code to confirm your identity:</p>" +
		"<h1>" + code + "</h1>" +
		"<p>Acknowledge that this code will expire at " + formattedExpirationTime + "</p>"

	subject := "Account creation"
	msg := formatEmailMessageHtml(subject, content)

	return Email.SendMail(to, subject, msg)
}

func EmailSendCodeForRecovery(to string, code string, expirationTime time.Time) error {
	formattedExpirationTime := expirationTime.Format("15:04:05")

	content := "<p>" +
		"It seems you have requested the recovery of your account on " +
		getWebsiteLinkHtml() + ". " +
		"To proceed, please use this code to confirm your identity:</p>" +
		"<h1>" + code + "</h1>" +
		"<p>Acknowledge that this code will expire at " + formattedExpirationTime + "</p>"

	subject := "Account recovery"
	msg := formatEmailMessageHtml(subject, content)

	return Email.SendMail(to, subject, msg)
}
