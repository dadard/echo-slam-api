package connectors_models

type MusicDto struct {
	Title       string `json:"title"`
	Artist      string `json:"artist"`
	Album       string `json:"album"`
	PublishedAt string `json:"published_at"`
	Genre       string `json:"genre"`
	ImageUrl    string `json:"image_url"`
}

type AlbumListResponse struct {
	Status bool `json:"Status"`
	Message string `json:"Message"`
	Content []AlbumDto `json:"Content"`
}

type ArtistListResponse struct {
	Status bool `json:"Status"`
	Message string `json:"Message"`
	Content []ArtistDto `json:"Content"`
}

type AlbumDto struct {
	Name string   `json:"name"`
	TitleList []string `json:"title_list"`
	Artist    string   `json:"artist"`
	ImageURL  string   `json:"image_url"`
}

type ArtistDto struct {
	Name string `json:"name"`
	AlbumList  []AlbumDto `json:"album_list"`
}
