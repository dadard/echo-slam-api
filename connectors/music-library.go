package connectors

import (
	"encoding/json"
	"gitlab.com/dadard/echo-slam-api/connectors/connectors_models"
	"net/http"
)

type MusicLibrary interface {
	// connector stuff
	Ping() error
	GetName() string

	AddToLibrary(token string, m connectors_models.MusicDto) (connectors_models.MusicDto, error)
	RemoveFromLibrary(token string, title string, artist string) (connectors_models.MusicDto, error)
	GetFromLibrary(token string, title string, artist string) (connectors_models.MusicDto, error)

	AlbumList(token string) ([]connectors_models.AlbumDto, error)
	ArtistList(token string) ([]connectors_models.ArtistDto, error)
}

func (c *Connector) AddToLibrary(token string, m connectors_models.MusicDto) (connectors_models.MusicDto, error) {
	var f connectors_models.MusicDto

	var headers = map[string]string{
		authorizationHeader: token,
	}

	body, err := json.Marshal(m)
	if err != nil {
		return f, err
	}

	var r connectors_models.MusicDto
	err = c.doRequest(http.MethodPost, "/library", nil, headers, body, &r)
	if err != nil {
		return f, err
	}

	return r, nil
}

func (c *Connector) RemoveFromLibrary(token string, title string, artist string) (connectors_models.MusicDto, error) {
	var f connectors_models.MusicDto

	var headers = map[string]string{
		authorizationHeader: token,
	}

	var params = map[string]string{
		"title":  title,
		"artist": artist,
	}

	var r connectors_models.MusicDto
	err := c.doRequest(http.MethodDelete, "/library", params, headers, nil, &r)
	if err != nil {
		return f, err
	}

	return r, nil

}

func (c *Connector) GetFromLibrary(token string, title string, artist string) (connectors_models.MusicDto, error) {
	var f connectors_models.MusicDto

	var headers = map[string]string{
		authorizationHeader: token,
	}

	var params = map[string]string{
		"title":  title,
		"artist": artist,
	}

	var r connectors_models.MusicDto
	err := c.doRequest(http.MethodGet, "/library", params, headers, nil, &r)
	if err != nil {
		return f, err
	}

	return r, nil
}

func (c *Connector) AlbumList(token string) ([]connectors_models.AlbumDto, error) {
	headers := map[string]string {
		authorizationHeader: token,
	}

	var res connectors_models.AlbumListResponse
	err := c.doRequest(http.MethodGet, "/library/albums", nil, headers, nil, &res)
	if err != nil {
		return nil, err
	}

	return res.Content, nil
}

func (c *Connector) ArtistList(token string) ([]connectors_models.ArtistDto, error) {
	headers := map[string]string {
		authorizationHeader: token,
	}

	var res connectors_models.ArtistListResponse
	err := c.doRequest(http.MethodGet, "/library/artists", nil, headers, nil, &res)
	if err != nil {
		return nil, err
	}

	return res.Content, nil
}
