# ECHO-SLAM-API

This API is an interface between user and music data, and the front web server `echo-slam.com`.

This is written in pure go, because go is awesome. Thank you, go.

## HOW DOES IT WORK

This API is just consuming others APIs.

Each of these deliver specific features (extract music, get metadatas...).


## API ACCESS

The access to this API is restricted (should be only be consumed by the front webservice).

However, you can use the others API separately. To do so, check out my [dev website](https://dadard.fr).
You'll find all the information you need.


