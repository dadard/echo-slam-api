package controllers

import (
	"gitlab.com/dadard/echo-slam-api/api"
	"gitlab.com/dadard/echo-slam-api/managers"
	"net/http"
)

// GET
// Authorization: 	None
// Params: 			None
// Body: 			None

// add to library
func LibraryPost(w http.ResponseWriter, r *http.Request) {
	username, err := getUsernameFromJwt(w, r)
	if err != nil {
		return
	}

	title := r.URL.Query().Get(titleParam)
	artist := r.URL.Query().Get(artistParam)

	if title == "" || artist == "" {
		api.Api.BuildMissingParameter(w)
		return
	}

	err = managers.LibraryAdd(username, title, artist)
	if err != nil {
		logger.Error(err.Error())
		api.Api.BuildErrorResponse(http.StatusInternalServerError, "failed to add to library", w)
		return
	}

	api.Api.BuildJsonResponse(true, "added to library", nil, w)

}

// DELETE
// Authorization: 	JWT
// Params: 			None
// Body: 			None

// remove from library
func LibraryDelete(w http.ResponseWriter, r *http.Request) {
	username, err := getUsernameFromJwt(w, r)
	if err != nil {
		return
	}

	title := r.URL.Query().Get(titleParam)
	artist := r.URL.Query().Get(artistParam)

	if title == "" || artist == "" {
		api.Api.BuildMissingParameter(w)
		return
	}

	err = managers.LibraryDelete(username, title, artist)
	if err != nil {
		logger.Error(err.Error())
		api.Api.BuildErrorResponse(http.StatusInternalServerError, "failed to remove music from library", w)
		return
	}

	api.Api.BuildJsonResponse(true, "music removed from library", nil, w)
}

// GET
// Authorization: 	JWT
// Params: 			None
// Body: 			None

// the list of albums in library
func LibraryAlbumsGet(w http.ResponseWriter, r *http.Request) {
	username, err := getUsernameFromJwt(w, r)
	if err != nil {
		return
	}

	l, err := managers.LibraryAlbumsList(username)
	if err != nil {
		logger.Error(err.Error())
		api.Api.BuildErrorResponse(http.StatusInternalServerError, "error getting album list", w)
		return
	}

	api.Api.BuildJsonResponse(true, "album list retrieved", l, w)
}

// GET
// Authorization: 	JWT
// Params: 			None
// Body: 			None

// the the list of artists in library
func LibraryArtistGet(w http.ResponseWriter, r *http.Request) {
	username, err := getUsernameFromJwt(w, r)
	if err != nil {
		return
	}

	l, err := managers.LibraryArtistList(username)
	if err != nil {
		logger.Error(err.Error())
		api.Api.BuildErrorResponse(http.StatusInternalServerError, "error getting artist list", w)
		return
	}

	api.Api.BuildJsonResponse(true, "artist list retrieved", l, w)
}

// POST
// Authorization: 	JWT
// Params: 			None
// Body: 			None

// download all library as zip file
func LibraryDownloadPost(w http.ResponseWriter, r *http.Request) {
	username, err := getUsernameFromJwt(w, r)
	if err != nil {
		return
	}

	p, err := managers.LibraryDownload(username)
	if err != nil {
		logger.Error(err.Error())
		api.Api.BuildErrorResponse(http.StatusInternalServerError, "error download library", w)
		return
	}

	// todo remove
	// for test
	api.Api.BuildJsonResponse(true, "library downloaded", p, w)

	// http.ServeFile(w, r, p)

}


// GET
// Authorization: 	None
// Params: 			fileTokenParam, usernameParam
// Body: 			None

// gives the zipfile
func LibraryDownloadFileGet(w http.ResponseWriter, r *http.Request) {
	token := r.URL.Query().Get(fileTokenParam)
	username := r.URL.Query().Get(usernameParam)
	if token == "" || username == "" {
		api.Api.BuildMissingParameter(w)
		return
	}

	zipPath, err := managers.LibraryDownloadFile(username, token)
	if err != nil {
		logger.Error(err.Error())
		api.Api.BuildErrorResponse(http.StatusInternalServerError, "error getting zip file", w)
		return
	}

	w.Header().Add("Access-Control-Allow-Origin", api.Api.Service.CorsOrigin())
	http.ServeFile(w, r, zipPath)
}
