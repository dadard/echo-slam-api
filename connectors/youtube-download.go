package connectors

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/dadard/echo-slam-api/connectors/connectors_models"
	"io"
	"net/http"
	"time"
)

type YoutubeDownload interface {
	// connector stuff
	Ping() error
	GetName() string

	// apis
	ListVideos(token string) ([]connectors_models.Video, error)
	AddVideo(token string, videoId string) (connectors_models.Video, error)
	AddVideoFromSearch(token string, title string, artist string,
		album string, genre string, date string) (connectors_models.Video, error)

	DeleteVideo(token string, videoId string) (connectors_models.Video, error)
	UpdateVideo(token string, videoId string, title string, artist string, album string,
		date time.Time, genre string, imageUrl string) (connectors_models.Video, error)

	AddPlaylist(token string, playlistId string) ([]connectors_models.Video, error)
	UpdateList(token string, videoIdList []string, title string,
		artist string, album string, date time.Time, genre string) ([]connectors_models.Video, error)
	DeleteList(token string, videoIdList []string) ([]connectors_models.Video, error)

	DownloadAll(token string) (connectors_models.HealthResponse, error)
	GetDownloadStatus(token string) (connectors_models.Job, error)
	GetZipFile(token string) (io.ReadCloser, error)
}

// apis
func (c *Connector) ListVideos(token string) ([]connectors_models.Video, error) {
	headers := map[string]string{
		authorizationHeader: token,
	}
	var r connectors_models.VideoListResponse
	err := c.doRequest(http.MethodGet, "/video/list", nil, headers, nil, &r)
	if err != nil {
		return nil, err
	}

	return r.Content, nil
}

func (c *Connector) AddVideo(token string, videoId string) (connectors_models.Video, error) {
	var f connectors_models.Video

	headers := map[string]string{
		authorizationHeader: token,
	}

	params := map[string]string{
		"videoId": videoId,
	}

	var v connectors_models.VideoResponse
	err := c.doRequest(http.MethodPost, "/video", params, headers, nil, &v)
	if err != nil {
		return f, err
	}

	return v.Content, nil
}

func (c *Connector) AddVideoFromSearch(token string, title string, artist string,
	album string, genre string, date string) (connectors_models.Video, error) {

	var headers = map[string]string{
		authorizationHeader: token,
	}

	var params = map[string]string{
		"title": title,
		"artist": artist,
		"album": album,
		"genre": genre,
		"date": date,
	}

	var res connectors_models.VideoResponse
	err := c.doRequest(http.MethodPost, "/video/search", params, headers, nil, &res)
	if err != nil {
		return connectors_models.Video{}, err
	}

	return res.Content, nil

}

func (c *Connector) DeleteVideo(token string, videoId string) (connectors_models.Video, error) {
	var f connectors_models.Video

	headers := map[string]string{
		authorizationHeader: token,
	}

	params := map[string]string{
		"videoId": videoId,
	}

	var v connectors_models.VideoResponse
	err := c.doRequest(http.MethodDelete, "/video", params, headers, nil, &v)
	if err != nil {
		return f, err
	}

	return v.Content, nil
}

func (c *Connector) UpdateVideo(token string, videoId string,
	title string, artist string, album string, date time.Time,
	genre string, imageUrl string) (connectors_models.Video, error) {

	var f connectors_models.Video

	headers := map[string]string{
		authorizationHeader: token,
		contentTypeHeader:   contentTypeJson,
	}

	dateParsed := fmt.Sprintf("%02d-%02d-%02d", date.Year(), date.Month(), date.Day())

	var vBody = connectors_models.Video{
		VideoID:  videoId,
		Title:    title,
		Album:    album,
		Artist:   artist,
		Date:     dateParsed,
		Genre:    genre,
		ImageUrl: imageUrl,
	}

	vData, err := json.Marshal(&vBody)
	if err != nil {
		return f, err
	}

	var v connectors_models.VideoResponse
	err = c.doRequest(http.MethodPut, "/video", nil, headers, vData, &v)
	if err != nil {
		return f, err
	}

	return v.Content, nil
}

func (c *Connector) AddPlaylist(token string, playlistId string) ([]connectors_models.Video, error) {
	headers := map[string]string{
		authorizationHeader: token,
	}

	params := map[string]string{
		"playlistId": playlistId,
	}

	var v connectors_models.VideoListResponse
	err := c.doRequest(http.MethodPost, "/video/list", params, headers, nil, &v)
	if err != nil {
		return nil, err
	}

	return v.Content, nil
}

func (c *Connector) UpdateList(token string, videoIdList []string, title string,
	artist string, album string, date time.Time, genre string) ([]connectors_models.Video, error) {

	headers := map[string]string{
		authorizationHeader: token,
	}

	dateParsed := fmt.Sprintf("%02d-%02d-%02d", date.Year(), date.Month(), date.Day())

	vBody := connectors_models.VideoInfosBody{
		VideoList: videoIdList,
		Infos: connectors_models.VideoInfosParams{
			Title:  title,
			Album:  album,
			Artist: artist,
			Date:   dateParsed,
			Genre:  genre,
		},
	}

	vData, err := json.Marshal(&vBody)
	if err != nil {
		return nil, err
	}

	var v connectors_models.VideoListResponse
	err = c.doRequest(http.MethodPut, "/video/list", nil, headers, vData, &v)
	if err != nil {
		return nil, err
	}

	return v.Content, nil
}

func (c *Connector) DeleteList(token string, videoIdList []string) ([]connectors_models.Video, error) {
	vbody := connectors_models.VideoListBody{
		VideoList: videoIdList,
	}

	vData, err := json.Marshal(&vbody)
	if err != nil {
		return nil, err
	}

	headers := map[string]string{
		authorizationHeader: token,
	}

	var v connectors_models.VideoListResponse
	err = c.doRequest(http.MethodDelete, "/video/list", nil, headers, vData, &v)
	if err != nil {
		return nil, err
	}

	return v.Content, nil
}

func (c *Connector) DownloadAll(token string) (connectors_models.HealthResponse, error) {
	var f connectors_models.HealthResponse
	headers := map[string]string{
		authorizationHeader: token,
	}

	var r connectors_models.HealthResponse
	err := c.doRequest(http.MethodPost, "/download/all", nil, headers, nil, &r)
	if err != nil {
		return f, err
	}

	return r, nil
}

func (c *Connector) GetDownloadStatus(token string) (connectors_models.Job, error) {
	var f connectors_models.Job

	headers := map[string]string{
		authorizationHeader: token,
	}

	var r connectors_models.JobResponse
	err := c.doRequest(http.MethodGet, "/download", nil, headers, nil, &r)
	if err != nil {
		return f, err
	}

	return r.Content, nil
}

func (c *Connector) GetZipFile(token string) (io.ReadCloser, error) {

	r, err := http.NewRequest(http.MethodGet, c.baseUrl+"/download/file?token="+token, nil)
	if err != nil {
		return nil, err
	}

	resp, err := c.client.Do(r)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, errors.New("invalid status from api: " + resp.Status)
	}

	return resp.Body, nil
}
