package repositories

import (
	"archive/zip"
	"crypto/sha256"
	"fmt"
	"gitlab.com/dadard/echo-slam-api/api"
	"gitlab.com/dadard/echo-slam-api/config"
	"gitlab.com/dadard/echo-slam-api/models"
	"io"
	"io/ioutil"
	"math/rand"
	"os"
	"path"
	"strconv"
	"strings"
	"time"
)

// private
const (
	maxProgress = 100
	minProgress = 0

	baseDirPlaceholder = "placeholder"
	mp3Extension       = "mp3"
)

// public
const (
	JobStatusCreated = "created"
	JobStatusEnded   = "ended"
)

func JobExists(username string) bool {
	_, err := JobGet(username)
	return err == nil
}

func jobGet(username string, done bool) (models.Job, error) {
	var f models.Job

	var j models.Job
	if done {
		api.Api.Database.Orm.Where(&models.Job{
			Username: username,
			Done:     true,
		}).Find(&j)
	} else {
		api.Api.Database.Orm.Where(&models.Job{
			Username: username,
		}).Last(&j)
	}

	if j.Username != username || j.Done != done {
		return f, config.ErrorEntityNotFound
	}

	return j, nil
}

// gives a running job for this user
func JobGet(username string) (models.Job, error) {
	return jobGet(username, false)
}

func JobDoneGet(username string) (models.Job, error) {
	return jobGet(username, true)
}

// create a new job
func JobCreate(username string, jobType string) (models.Job, error) {
	var f models.Job

	// check for existing running job
	if JobExists(username) {
		return f, config.ErrorJobExists
	}

	api.Api.Database.Orm.Create(&models.Job{
		Username:  username,
		Type:      jobType,
		Progress:  minProgress,
		Done:      false,
		Status:    JobStatusCreated,
		StartedAt: time.Now(),
		EndedAt:   time.Time{},
	})

	// check job has been created
	j, err := JobGet(username)
	if err != nil {
		logger.Error(err.Error())
		return f, config.ErrorUnexpectedDb
	}

	return j, nil
}

func JobUpdateProgress(username string, progress int, status string) (models.Job, error) {
	var f models.Job

	if progress > maxProgress || progress < minProgress {
		return f, config.ErrorInvalidProgress
	}

	j, err := JobGet(username)
	if err != nil {
		return f, config.ErrorEntityNotFound
	}

	j.Progress = progress
	j.Status = status

	api.Api.Database.Orm.Save(&j)

	return j, nil
}

func JobEnd(username string) (models.Job, error) {
	var f models.Job

	j, err := JobGet(username)
	if err != nil {
		return f, config.ErrorEntityNotFound
	}

	j.Progress = maxProgress
	j.Done = true
	j.Status = JobStatusEnded
	j.EndedAt = time.Now()

	api.Api.Database.Orm.Save(&j)

	return j, nil
}

func getRandomHash() string {
	seed := time.Now().UnixNano()
	rand.Seed(seed)
	randInt := rand.Int()
	randBytes := []byte(strconv.Itoa(randInt))
	hash := sha256.New()
	hash.Write(randBytes)
	key := fmt.Sprintf("%x", hash.Sum(nil))
	return key
}

// create new placeholder, return placeholder path
func initPlaceholder() (string, error) {
	h := getRandomHash()
	p := path.Join(baseDirPlaceholder, h)
	if err := os.Mkdir(p, 0755); err != nil {
		return "", err
	}
	return h, nil
}

// delete placeholder
func deletePlaceholder(h string) error {
	p := path.Join(baseDirPlaceholder, h)
	// check it exists
	_, err := ioutil.ReadDir(p)
	if err != nil {
		return err
	}

	return os.RemoveAll(p)
}

// get output file path
func getOutputZipFilePath(h string) string {
	return path.Join(baseDirPlaceholder, h, "files.zip")
}

// get placeholder path
func getPlaceholderPath(h string) string {
	return path.Join(baseDirPlaceholder, h)
}

// store content of zipfile
func StoreZipFile(body io.ReadCloser) (string, error) {
	h, err := initPlaceholder()
	if err != nil {
		return "", err
	}

	zipFile := getOutputZipFilePath(h)
	out, err := os.Create(zipFile)
	if err != nil {
		return "", err
	}

	logger.Debug("writing zip file into placeholder " + zipFile)
	dataCount, err := io.Copy(out, body)
	if err != nil {
		return "", err
	}

	logger.Debug(fmt.Sprintf("written %d data", dataCount))
	err = body.Close()
	if err != nil {
		return "", err
	}

	return h, nil
}

// extract zip file in placeholder
func ExtractZipFile(h string) error {
	r, err := zip.OpenReader(getOutputZipFilePath(h))
	if err != nil {
		return err
	}

	for _, f := range r.File {
		outputFile := path.Join(getPlaceholderPath(h), f.Name)
		output, err := os.Create(outputFile)
		if err != nil {
			logger.Warning(err.Error())
			continue
		}

		rc, err := f.Open()
		if err != nil {
			logger.Warning(err.Error())
			continue
		}

		_, err = io.Copy(output, rc)
		if err != nil {
			logger.Warning(err.Error())
			continue
		}

		err = rc.Close()
		if err != nil {
			logger.Warning(err.Error())
			continue
		}
	}

	return nil
}

// get the list of mp3s from zip
func GetMp3List(h string) ([]string, error) {
	placeholder := getPlaceholderPath(h)
	d, err := ioutil.ReadDir(placeholder)
	if err != nil {
		return nil, err
	}

	mp3List := make([]string, 0)
	for _, f := range d {
		// check extension
		extension := strings.Split(f.Name(), ".")[1]
		if extension == mp3Extension {
			mp3List = append(mp3List, path.Join(placeholder, f.Name()))
		}
	}

	return mp3List, nil
}

// get the mp3 file from title, to be adapted from youtube-dl
func GetMp3File(h string, m models.Music) (string, error) {
	p := getPlaceholderPath(h)

	// /!\ format from youtube-dl /!\
	filename := fmt.Sprintf("%s.%s", m.Title, mp3Extension)

	filePath := path.Join(p, filename)
	_, err := os.Open(filePath)
	if err != nil {
		return "", err
	}

	return filePath, nil
}

func ClearPlaceholder(h string) error {
	logger.Debug("placeholder " + h + " cleared")

	p := getPlaceholderPath(h)
	return os.RemoveAll(p)
}
