package connectors

import (
	"fmt"
	"os"
	"testing"
)

func newLibrary() *Connector {
	return newConnector("music-library", "http://localhost:8085")
}

func getLibraryToken() string {
	return os.Getenv("LIBRARY_TOKEN")
}

func TestConnector_AlbumList(t *testing.T) {
	var c MusicLibrary = newLibrary()

	l, err := c.AlbumList(getLibraryToken())
	if err != nil {
		t.Error(err)
	}
	fmt.Print(l)

}

func TestConnector_ArtistList(t *testing.T) {
	var c MusicLibrary = newLibrary()
	token := getLibraryToken()
	fmt.Println(t)

	l, err := c.ArtistList(token)
	if err != nil {
		t.Error(err)
	}
	fmt.Print(l)

}