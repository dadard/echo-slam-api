package repositories

import (
	"gitlab.com/dadard/echo-slam-api/api"
	"gitlab.com/dadard/echo-slam-api/config"
	"gitlab.com/dadard/echo-slam-api/models"
)

func ProfileExists(username string) bool {
	_, err := ProfileGet(username)
	return err == nil
}

func ProfileGet(username string) (models.ProfileEntity, error) {
	var fallback models.ProfileEntity

	var p models.ProfileEntity
	api.Api.Database.Orm.Where(&models.ProfileEntity{
		Username: username,
	}).First(&p)

	if p.Username != username {
		return fallback, config.ErrorEntityNotFound
	}

	return p, nil
}

func ProfileCreate(p models.ProfileEntity) (models.ProfileEntity, error) {
	var fallback models.ProfileEntity

	api.Api.Database.Orm.Create(&p)

	if !ProfileExists(p.Username) {
		return fallback, config.ErrorUnexpectedDb
	}

	return p, nil
}

func ProfileUpdate(p models.ProfileEntity) {
	api.Api.Database.Orm.Save(&p)
}

func ProfileDelete(username string) error {
	p, err := ProfileGet(username)
	if err != nil {
		return err
	}

	api.Api.Database.Orm.Delete(&p)

	return nil
}

func ProfileList(limit int) []models.ProfileEntity {
	var res []models.ProfileEntity
	api.Api.Database.Orm.Order("date_connected desc").Limit(limit).Find(&res)

	return res
}
