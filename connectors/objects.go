package connectors

import (
	coreConnectors "github.com/Dadard29/go-core/connectors"
	"net/http"
)

type Connector struct {
	Name    string
	client  *http.Client
	baseUrl string

	healthRoute string
	infosRoute  string
}

var (
	Telegram *coreConnectors.TelegramConnector
	Email    *coreConnectors.MailConnector

	CoreConnector            Core
	YoutubeDownloadConnector YoutubeDownload
	MusicResearcherConnector MusicResearcher
	WarehouseConnector       Warehouse
	MusicLibraryConnector    MusicLibrary
)
