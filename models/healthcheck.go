package models

// dto
type HealthCheckDto struct {
	ApiName string `json:"api_name"`
	Check   bool   `json:"check"`
}

type HealthCheckListDto struct {
	List []HealthCheckDto `json:"list"`
}
