package connectors

import (
	"bytes"
	"encoding/json"
	coreConnectors "github.com/Dadard29/go-core/connectors"
	"gitlab.com/dadard/echo-slam-api/api"
	"gitlab.com/dadard/echo-slam-api/config"
	"gitlab.com/dadard/echo-slam-api/connectors/connectors_models"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
)

const (
	authorizationHeader = "X-Access-Token"
	contentTypeHeader   = "Content-Type"
	contentTypeJson     = "application/json"
)

func deserialize(resp *http.Response, respObj interface{}) error {
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	return json.Unmarshal(data, respObj)
}

// respObj is a pointer of the output object (json decoded)
func (c Connector) doRequest(method string, route string, params map[string]string,
	headers map[string]string, body []byte, respObj interface{}) error {
	var err error

	req, err := http.NewRequest(method, c.baseUrl+route, bytes.NewBuffer(body))
	if err != nil {
		return err
	}

	if headers != nil {
		for k, v := range headers {
			req.Header.Add(k, v)
		}
	}

	if params != nil {
		q, err := url.ParseQuery(req.URL.RawQuery)
		if err != nil {
			return err
		}
		for k, v := range params {
			q.Add(k, v)
		}
		req.URL.RawQuery = q.Encode()
	}

	resp, err := c.client.Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return config.ErrorInvalidStatusCode
	}

	return deserialize(resp, respObj)
}

func newConnector(apiName string, host string) *Connector {
	return &Connector{
		client:      &http.Client{},
		baseUrl:     host,
		healthRoute: "/health",
		infosRoute:  "/infos",
		Name:        apiName,
	}
}

func newConnectorFromConfig(subCategory string) (*Connector, error) {
	var err error

	apiConfig, err := api.Api.Config.GetSubcategoryFromFile(
		config.Connectors,
		subCategory)
	if err != nil {
		return nil, err
	}

	return newConnector(subCategory, apiConfig["host"]), nil
}

// init communication connectors (telegram, mail)
func setComConnectors() error {
	var err error

	// telegram
	botTokenKey, err := api.Api.Config.GetValueFromFile(
		config.Connectors,
		config.ConnectorsTelegram,
		"botTokenKey")
	if err != nil {
		return err
	}

	botToken := api.Api.Config.GetEnv(botTokenKey)
	Telegram, err = coreConnectors.NewTelegramConnector(botToken)
	if err != nil {
		return err
	}

	// email
	mailConfig, err := api.Api.Config.GetSubcategoryFromFile(
		config.Connectors,
		config.ConnectorsEmail)
	if err != nil {
		return err
	}

	smtpUser := api.Api.Config.GetEnv(mailConfig["user"])
	smtpPassword := api.Api.Config.GetEnv(mailConfig["password"])
	port, err := strconv.Atoi(mailConfig["port"])
	if err != nil {
		return err
	}

	Email = coreConnectors.NewMailConnector(mailConfig["from"],
		coreConnectors.SmtpConfig{
			Host:     mailConfig["host"],
			Port:     port,
			User:     smtpUser,
			Password: smtpPassword,
		})

	return nil
}

// init global apis connectors objects (youtube-download, core...)
func setApiConnectors() error {
	var err error

	// core
	if CoreConnector, err = newConnectorFromConfig(
		config.ConnectorsCore); err != nil {
		return err
	}

	// youtube-download
	if YoutubeDownloadConnector, err = newConnectorFromConfig(
		config.ConnectorsYoutubeDownload); err != nil {
		return err
	}

	// music-researcher
	if MusicResearcherConnector, err = newConnectorFromConfig(
		config.ConnectorsMusicResearcher); err != nil {
		return err
	}

	// warehouse
	if WarehouseConnector, err = newConnectorFromConfig(
		config.ConnectorsWarehouse); err != nil {
		return err
	}

	// library
	if MusicLibraryConnector, err = newConnectorFromConfig(
		config.ConnectorsMusicLibrary); err != nil {
		return err
	}

	return nil
}

// exposed
func SetConnectors() error {
	if err := setComConnectors(); err != nil {
		return err
	}

	if err := setApiConnectors(); err != nil {
		return err
	}

	return nil
}

func (c *Connector) Ping() error {
	var h connectors_models.HealthResponse
	if err := c.doRequest(
		http.MethodGet, c.healthRoute, nil, nil, nil, &h); err != nil {
		return err
	}

	if !h.Status {
		return config.ErrorPingFailed
	}
	return nil
}

func (c *Connector) GetName() string {
	return c.Name
}
