package connectors_models

import "time"

type VideoListResponse struct {
	Content []Video `json:"Content"`
}

type VideoResponse struct {
	Content Video `json:"Content"`
}

type Video struct {
	VideoID  string `json:"VideoId"`
	Title    string `json:"Title"`
	Album    string `json:"Album"`
	Artist   string `json:"Artist"`
	Date     string `json:"Date"`
	Genre    string `json:"Genre"`
	ImageUrl string `json:"ImageUrl"`
}

func (v Video) GetDate() (time.Time, error) {
	return time.Parse("2006-01-02 00:00:00 +0000 UTC", v.Date)
}

type VideoListBody struct {
	VideoList []string `json:"VideoList"`
}

type VideoInfosParams struct {
	Title  string `json:"Title"`
	Album  string `json:"Album"`
	Artist string `json:"Artist"`
	Date   string `json:"Date"`
	Genre  string `json:"Genre"`
}

type VideoInfosBody struct {
	VideoList []string         `json:"VideoList"`
	Infos     VideoInfosParams `json:"Infos"`
}
