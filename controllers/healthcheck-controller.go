package controllers

import (
	"gitlab.com/dadard/echo-slam-api/api"
	"gitlab.com/dadard/echo-slam-api/managers"
	//"gitlab.com/dadard/echo-slam-api/models"

	//"gitlab.com/dadard/echo-slam-api/models"

	"net/http"
)

// GET
// Authorization: 	None
// Params: 			None
// Body: 			None

// check all connectors
func HealthCheckApisGet(w http.ResponseWriter, r *http.Request) {
	l, msg, err := managers.HealthManagerApiList()
	if err != nil {
		api.Api.BuildErrorResponse(http.StatusInternalServerError, msg, w)
		return
	}

	status := true
	for _, v := range l.List {
		if !v.Check {
			status = false
			break
		}
	}

	api.Api.BuildJsonResponse(status, "healthcheck done", l, w)
	return
}
