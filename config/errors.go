package config

import "errors"

var (
	ErrorEmptyParam        = errors.New("empty param")
	ErrorWrongAuthFormat   = errors.New("wrong auth format")
	ErrorPasswordWeak      = errors.New("password too weak (length < 8)")
	ErrorInvalidRecoverBy  = errors.New("invalid recover by")
	ErrorWrongPassword     = errors.New("invalid password")
	ErrorWrongCode         = errors.New("wrong code")
	ErrorAvatarNotFound    = errors.New("avatar not found")
	ErrorInvalidUrl        = errors.New("invalid youtube URL")
	ErrorInvalidStatusCode = errors.New("invalid status code from api")

	ErrorPingFailed  = errors.New("ping failed: status of api is False")
	ErrorDbDown      = errors.New("database down")
	ErrorCodeExpired = errors.New("code expired")

	ErrorEntityNotFound      = errors.New("entity not found")
	ErrorUsernameAlreadyUsed = errors.New("username already used")
	ErrorUnexpectedDb        = errors.New("unexpected error occurred when interacting with DB")
	ErrorJobExists           = errors.New("a job is already running for this user")
	ErrorInvalidProgress     = errors.New("invalid progress, must be > 0 and < 100")
	ErrorMusicAlreadyExists = errors.New("this music is already available")
)
