package managers

import (
	"gitlab.com/dadard/echo-slam-api/api"
	"gitlab.com/dadard/echo-slam-api/config"
	"io/ioutil"
	"path"
	"strings"
)

const (
	avatarExtension = ".png"
)

func AvatarManagerGet(name string) (string, string, error) {
	var fallback = ""
	var msg = "error getting avatar"

	baseDir, err := api.Api.Config.GetValueFromFile(
		config.Profile,
		config.ProfileAvatar,
		"baseDir")
	if err != nil {
		return fallback, msg, err
	}

	l, m, err := AvatarManagerList()
	if err != nil {
		return fallback, m, err
	}

	for _, v := range l {
		if v == name {
			return path.Join(baseDir, v+avatarExtension), "avatar retrieved", nil
		}
	}

	return fallback, msg, config.ErrorAvatarNotFound

}

func AvatarManagerList() ([]string, string, error) {
	var fallback []string
	var msg = "error getting avatar list"

	baseDir, err := api.Api.Config.GetValueFromFile(
		config.Profile,
		config.ProfileAvatar,
		"baseDir")
	if err != nil {
		return fallback, msg, err
	}

	baseDirRead, err := ioutil.ReadDir(baseDir)
	if err != nil {
		return fallback, msg, err
	}

	var res = make([]string, 0)
	for _, a := range baseDirRead {
		if a.IsDir() {
			continue
		}

		fn := strings.Split(a.Name(), avatarExtension)[0]
		res = append(res, fn)
	}

	return res, "avatar list retrieved", nil
}
