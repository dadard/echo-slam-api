package connectors_models

type SpotifyResponse struct {
	Message string    `json:"Message"`
	Content []Spotify `json:"Content"`
}

type Spotify struct {
	ID         string   `json:"ID"`
	Title      string   `json:"Title"`
	Artist     string   `json:"Artist"`
	Album      string   `json:"Album"`
	Date       string   `json:"Date"`
	Genre      []string `json:"Genre"`
	ImageURL   string   `json:"ImageURL"`
	PreviewURL string   `json:"PreviewURL"`
}
