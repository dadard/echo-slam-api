package controllers

import (
	"gitlab.com/dadard/echo-slam-api/api"
	"gitlab.com/dadard/echo-slam-api/managers"
	"net/http"
)

// PUT
// Authorization: 	JWT
// Params: 			titleParam, artistParam, albumParam
// Body: 			None
func PlayerPut(w http.ResponseWriter, r *http.Request) {
	username, err := getUsernameFromJwt(w, r)
	if err != nil {
		return
	}

	title := r.URL.Query().Get(titleParam)
	artist := r.URL.Query().Get(artistParam)
	album := r.URL.Query().Get(albumParam)
	url := r.URL.Query().Get(urlParam)

	if title == "" || artist == "" || album == "" || url == "" {
		api.Api.BuildMissingParameter(w)
		return
	}

	p, err := managers.PlayerManagerUpdate(username, title, artist, album, url)
	if err != nil {
		logger.Error(err.Error())
		api.Api.BuildErrorResponse(http.StatusInternalServerError, "failed to update the player", w)
		return
	}
	api.Api.BuildJsonResponse(true, "player updated", p, w)
}
