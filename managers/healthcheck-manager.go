package managers

import (
	"gitlab.com/dadard/echo-slam-api/connectors"
	"gitlab.com/dadard/echo-slam-api/models"
)

func HealthManagerApiList() (models.HealthCheckListDto, string, error) {
	var h models.HealthCheckListDto
	var check bool

	// -- youtube-download
	if err := connectors.YoutubeDownloadConnector.Ping(); err != nil {
		logger.Error(err.Error())
		check = false
	} else {
		check = true
	}
	h.List = append(h.List, models.HealthCheckDto{
		ApiName: connectors.YoutubeDownloadConnector.GetName(),
		Check:   check,
	})

	// -- music researcher
	if err := connectors.MusicResearcherConnector.Ping(); err != nil {
		logger.Error(err.Error())
		check = false
	} else {
		check = true
	}
	h.List = append(h.List, models.HealthCheckDto{
		ApiName: connectors.MusicResearcherConnector.GetName(),
		Check:   check,
	})

	// -- music library
	if err := connectors.MusicLibraryConnector.Ping(); err != nil {
		logger.Error(err.Error())
		check = false
	} else {
		check = true
	}
	h.List = append(h.List, models.HealthCheckDto{
		ApiName: connectors.MusicLibraryConnector.GetName(),
		Check:   check,
	})

	// -- warehouse
	check = true
	if err := connectors.WarehouseConnector.Ping(); err != nil {
		logger.Error(err.Error())
		check = false
	}

	if err := connectors.WarehouseConnector.HealthConflicts(getWarehouseToken()); err != nil {
		logger.Error(err.Error())
		check = false
	}

	h.List = append(h.List, models.HealthCheckDto{
		ApiName: connectors.WarehouseConnector.GetName(),
		Check:   check,
	})

	// -- CORE
	if err := connectors.CoreConnector.Ping(); err != nil {
		logger.Error(err.Error())
		check = false
	} else {
		check = true
	}
	h.List = append(h.List, models.HealthCheckDto{
		ApiName: connectors.CoreConnector.GetName(),
		Check:   check,
	})

	return h, "ping performed", nil
}
