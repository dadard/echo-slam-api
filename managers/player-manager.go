package managers

import (
	"gitlab.com/dadard/echo-slam-api/models"
	"gitlab.com/dadard/echo-slam-api/repositories"
)

func PlayerManagerUpdate(username string, title string, artist string, album string, url string) (models.ProfileDto, error) {
	var f models.ProfileDto

	p, err := repositories.ProfileGet(username)
	if err != nil {
		return f, err
	}

	p.PlayingTitle = title
	p.PlayingArtist = artist
	p.PlayingAlbum = album
	p.PlayingUrl = url

	repositories.ProfileUpdate(p)

	return p.ToDto(), nil
}
