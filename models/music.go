package models

import "time"

// generic type for music object
type Music struct {
	Title  string
	Album  string
	Artist string

	PublishedAt string
	ImageUrl    string
	Genres      string

	// tells if it can be downloaded from the warehouse right now or not
	Available bool
	InLibrary bool

	// if in warehouse, date of addition and by who
	AddedAt time.Time
	Url     string
}

func (m Music) CheckSanity() bool {
	return m.Title != "" && m.Album != "" && m.Artist != "" &&
		m.PublishedAt != "" && m.ImageUrl != "" && m.Genres != ""
}

func (m Music) GetPublishedTime() (time.Time, error) {
	if len(m.PublishedAt) == 4 {
		// then, its a year only: 2006
		t, err := time.Parse("2006", m.PublishedAt)
		if err != nil {
			return time.Time{}, err
		}

		return t, nil
	} else {
		t, err := time.Parse("2006-01-02", m.PublishedAt)
		if err != nil {
			return time.Time{}, err
		}

		return t, nil
	}
}
