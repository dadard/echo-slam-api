package repositories

import (
	"archive/zip"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path"
	"strings"
)

func libraryDownloadGetPlaceholderPath(h string, token string) string {
	return path.Join(baseDirPlaceholder, token, h)
}

func libraryDownloadGetMp3FilePath(h string, token string,
	title string, album string, artist string) string {
	filename := fmt.Sprintf("%s.mp3", title)

	return path.Join(baseDirPlaceholder, token, h, filename)
}

func LibraryDownloadGetZipFilePath(h string, token string) string {
	return path.Join(baseDirPlaceholder, token, h, "library.zip")
}

// create a private placeholder
func LibraryDownloadInitPlaceholder(token string) (string, error) {
	var f string

	hash := getRandomHash()
	d, err := ioutil.ReadDir(baseDirPlaceholder)
	if err != nil {
		return f, err
	}

	for _, dd := range d {
		if dd.IsDir() && dd.Name() == token {
			// placeholder exists for this token
			h, err := ioutil.ReadDir(path.Join(baseDirPlaceholder, dd.Name()))
			if err != nil {
				return f, err
			}

			for _, hh := range h {
				err := os.RemoveAll(path.Join(baseDirPlaceholder, dd.Name(), hh.Name()))
				if err != nil {
					return f, err
				}
			}

			newPlaceholderPath := path.Join(baseDirPlaceholder, dd.Name(), hash)
			err = os.Mkdir(newPlaceholderPath, 0755)
			if err != nil {
				return f, err
			}

			return hash, nil
		}
	}

	// in that case, placeholder does not exist yet
	privatePath := path.Join(baseDirPlaceholder, token)
	err = os.Mkdir(privatePath, 0755)
	if err != nil {
		return f, err
	}

	newPlaceholderPath := path.Join(baseDirPlaceholder, token, hash)
	err = os.Mkdir(newPlaceholderPath, 0755)

	if err != nil {
		return f, err
	}

	return hash, err
}

func LibraryDownloadStoreMp3File(h string, token string, body io.ReadCloser,
	title string, album string, artist string) error {

	mp3Filepath := libraryDownloadGetMp3FilePath(h, token, title, album, artist)
	out, err := os.Create(mp3Filepath)
	if err != nil {
		return err
	}

	_, err = io.Copy(out, body)
	if err != nil {
		return err
	}
	err = body.Close()
	if err != nil {
		return err
	}

	return nil
}

func LibraryDownloadZipMp3Files(h string, token string) (string, error) {
	var f string

	zipFilePath := LibraryDownloadGetZipFilePath(h, token)
	zipFile, err := os.Create(zipFilePath)
	if err != nil {
		return f, err
	}

	archive := zip.NewWriter(zipFile)

	dir2Read := libraryDownloadGetPlaceholderPath(h, token)
	dir, err := ioutil.ReadDir(dir2Read)
	if err != nil {
		return f, err
	}

	for _, d := range dir {
		if d.IsDir() || !strings.Contains(d.Name(), ".mp3") {
			continue
		}

		file, err := archive.Create(d.Name())
		if err != nil {
			return f, err
		}

		originFile, err := os.OpenFile(path.Join(dir2Read, d.Name()), os.O_RDONLY, 0644)
		if err != nil {
			return f, err
		}

		body, err := ioutil.ReadAll(originFile)
		if err != nil {
			return f, err
		}

		_, err = file.Write(body)
		if err != nil {
			return f, err
		}
	}

	err = archive.Close()
	if err != nil {
		return f, err
	}

	return zipFilePath, nil
}

func LibraryDownloadCleanMp3Files(h string, token string) error {
	p := libraryDownloadGetPlaceholderPath(h, token)
	dir, err := ioutil.ReadDir(p)
	if err != nil {
		return err
	}

	for _, f := range dir {
		if f.IsDir() || !strings.Contains(f.Name(), ".mp3") {
			continue
		}

		err := os.Remove(path.Join(baseDirPlaceholder, token, h, f.Name()))
		if err != nil {
			return err
		}
	}

	return nil
}
