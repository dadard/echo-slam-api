package connectors

import (
	"gitlab.com/dadard/echo-slam-api/connectors/connectors_models"
	"net/http"
)

const (
	queryParam = "q"
)

type MusicResearcher interface {
	// connector stuff
	Ping() error
	GetName() string

	SearchYoutube()
	SearchSpotify(token string, query string) ([]connectors_models.Spotify, error)
}

func (c Connector) SearchYoutube() {

}

func (c *Connector) SearchSpotify(token string, query string) ([]connectors_models.Spotify, error) {

	params := map[string]string{
		"q": query,
	}

	headers := map[string]string{
		authorizationHeader: token,
	}

	var res connectors_models.SpotifyResponse
	err := c.doRequest(http.MethodGet, "/search/spotify", params, headers, nil, &res)
	if err != nil {
		return nil, err
	}

	return res.Content, nil
}
