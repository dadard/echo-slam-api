package managers

import (
	"os"
	"testing"
)

func TestManager_uploadAll(t *testing.T) {
	token := os.Getenv("YT_TOKEN")
	err := uploadAll(token, "", "")
	if err != nil {
		t.Error(err)
	}
}
